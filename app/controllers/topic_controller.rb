class TopicController < ApplicationController

  before_filter :find_topic, :only => :view
  before_filter :can_view_topics?, :only => :view
  before_filter :find_post, :only => [:edit, :update]
  before_filter :construct_post, :only => [:new, :create]
  before_filter :can_view_posts?, :except => :view
  before_filter :can_post_topics?, :only => [:new, :create]
  before_filter :can_post_replies?, :only => [:new, :create]
  before_filter :flood?, :only => [:create, :update]
  before_filter :can_edit_posts?, :only => [:edit, :update]
  before_filter :can_delete_posts?, :only => [:delete, :destroy]

  def view
    @post_pages, @posts = paginate_association(@topic, :posts, {}, @per_page)
    raise ParameterInvalid if @posts.empty?
  end

  def create
    if params.key? 'preview'
      render :action => 'new'
      return
    end
    @post.save!
    redirect_to :action => 'view', :id => @post.topic.id, :anchor => "p#{@post.id}"
  rescue ActiveRecord::RecordInvalid
    flash[:error] = @post.errors
    render :action => 'new'
  end

  def update
    @post.attributes = params[:post]
    if params.key? 'preview'
      render :action => 'new'
      return
    end
    @post.save!
    redirect_to :action => 'view', :id => @post.topic.id, :anchor => "p#{@post.id}"
  rescue ActiveRecord::RecordInvalid
    flash[:error] = @post.errors
    render :action => 'edit'
  end

  protected

    def find_post
      @post = Post.find params[:pid]
    end

    def construct_post
      cu = current_user
      @post = Post.new(params[:post])
      @post.poster_user = cu
      @post.poster_ip = request.env['REMOTE_ADDR']
      if params.key? :id
        @post.topic = Topic.find params[:id] 
      else
        @post.build_topic(params[:topic])
        @post.topic.forum = Forum.find params[:fid]
      end
      @post.topic.poster = cu.username
      @post.topic.last_post = ApplicationController.now
      @post.message = quoted_message(Post.find(params[:qid])) unless params[:qid].nil?
    end

    def quoted_message(post)
      "#{post.poster} wrote:\n\n" + post.message.gsub(/^/,'> ') + "\n\n"
    end

    def can_post_topics?
      return true if @post.topic.forum.can_post? current_user
      raise PermissionException, 'You are not allowed to post new topics'
    end

    def can_post_replies?
      # if it's not a reply, forget it
      return true if params[:id].nil?
      return true if @post.topic.can_reply? current_user
      raise PermissionException, 'You are not allowed to post replies'
    end

    def flood?
      cu = current_user
      return true if cu.is_guest? or params[:preview] != nil
      return true if !cu.is_flood? ApplicationController.now
      raise PermissionException, "You have to wait for at least #{cu.group.g_post_flood} seconds before posting another message"
    end

    def can_delete_posts?
      return true if @post.can_delete? current_user
      raise PermissionException, 'You are not allowed to edit posts'
    end

    def can_edit_posts?
      return true if @post.can_edit? current_user
      raise PermissionException, 'You are not allowed to edit posts'
    end

    def find_topic
      @per_page = current_user.get_disp_posts
      if params.key? 'pid'
        post = Post.find params[:pid]
        @topic = post.topic
        p = @topic.get_post_position(post)
        params[:page] = p / @per_page + 1
      end
      @topic = Topic.find params[:id] if @topic.nil?
    end

    def can_view_topics?
      raise PermissionException unless @topic.forum.can_view? current_user
    end

    def can_view_posts?
      raise PermissionException unless @post.topic.forum.can_view? current_user
    end
end
