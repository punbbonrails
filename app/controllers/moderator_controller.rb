require 'socket'

class ModeratorController < ApplicationController
  before_filter :login_required
  before_filter :can_moderate
  before_filter :find_topic, :except => [:get_host, :forum, :destroy_topics]

  def get_host
    ip = params[:id]
    flash[:notice] = "Hostname of #{ip} is: "+Socket.gethostbyaddr((ip.split(/\./).map {|x| x.to_i}.pack("CCCC")))[0]
  rescue SocketError
    flash[:error] = 'Host not found'
  end

  def stick
    @topic.sticky = true
    @topic.save!
    flash[:notice] = 'Topic sticked'
    redirect_to :controller => 'topic', :action => 'view', :id => @topic.id
  end

  def unstick
    @topic.sticky = false
    @topic.save!
    flash[:notice] = 'Topic unsticked'
    redirect_to :controller => 'topic', :action => 'view', :id => @topic.id
  end

  def close
    @topic.closed = true
    @topic.save!
    flash[:notice] = 'Topic closed'
    redirect_to :controller => 'topic', :action => 'view', :id => @topic.id
  end

  def open
    @topic.closed = false
    @topic.save!
    flash[:notice] = 'Topic opened'
    redirect_to :controller => 'topic', :action => 'view', :id => @topic.id
  end

  def destroy_topics
    topic_ids = params[:topics].split(/,/).map {|x| x.to_i}
    for id in topic_ids
      t = Topic.find id
      t.destroy
    end
    flash[:notice] = 'Topics deleted'
    redirect_to :controller => 'forum', :action => 'view', :id => params[:fid]
  end

  def forum
    @topic_ids = params[:topics]
    if @topic_ids.is_a? Array
      if params.key? :open 
        Topic.set_close_status(topic_ids, false)
      end
      if params.key? :close
        Topic.set_close_status(topic_ids, true)
      end
      if params.key? :delete_topics
        render :action => 'delete'
        return
      end
    end
    redirect_to :controller => 'forum', :action => 'view', :id => params[:id], :moderate => true
  end

  protected

    def find_topic
      @topic = Topic.find params[:id]
      raise PermissionException unless current_user.can_moderate_forum? @topic.forum
    end
end
