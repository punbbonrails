class UserController < ApplicationController
  helper :user
  skip_before_filter :can_read_board?, :only => [:logout,:login]
  before_filter :can_search_users?, :only => :list
  before_filter :can_register?, :only => [:new,:create]

  def list
    conditions = []
    vars = {}
    unless params[:username].nil? or params[:username].blank?
      if params[:username] =~ /\*/
        conditions.push('username LIKE :username')
        vars[:username] = params[:username].gsub(/\*/,'%')
      else
        conditions.push('username = :username')
        vars[:username] = params[:username]
      end
    end
    unless params[:show_group].nil? or params[:show_group].blank?
      i = params[:show_group].to_i
      if i != -1
        conditions.push('group_id = :group_id')
        vars[:group_id] = i
      end
    end
    sort_by = 'username'
    sort_by = params[:sort_by] if ['username','created_at','num_posts'].member? params[:sort_by]
    sort_dir = 'ASC'
    sort_dir = 'DESC' if params[:sort_dir] == 'DESC'
    p = {:order => "#{sort_by} #{sort_dir}"}
    p[:conditions] =  [conditions.join(' AND '), vars] unless conditions.empty?
    @user_pages, @users = paginate :user, p
    @groups = Group.find :all, :conditions => "id != #{Settings.group_ids[:pun_guest]}", :order => 'g_title'
  end

  def view
    @user = User.find params[:id]
  end

  def new
    redirect_to :controller => 'forum' and return if params.key? :cancel
    render :action => 'rules' and return if Settings.rules == true and params[:agree].nil?
    @user = User.new
  end

  def create
    @user = User.new(params[:user])
    @user.username = params[:user][:username]
    @user.save!
    flash[:notice] = 'User registered'
    redirect_to :controller => 'forum'
  rescue ActiveRecord::RecordInvalid
    flash[:error] = @user.errors
    render :action => 'new'
  end

  def logout
    self.current_user = nil
    flash[:notice] = "You have been logged out."
    redirect_back_or_default :controller => 'forum'
  end

  def login
    return unless request.post?
    self.current_user = User.authenticate(params[:username], params[:password])
    if !current_user.is_guest?
      flash[:notice] = "Logged in successfully"
      redirect_back_or_default :controller => 'forum'
    else
      flash[:error] = "Wrong username or password"
    end
  end

  protected

    def can_search_users?
      cu = current_user
      return true if cu.is_a?(User) and cu.is_admin?
      return true if cu.is_a?(User) and cu.group.g_search_users == true
      raise PermissionException, 'No permission to search for users'
    end

    def can_register?
      raise PermissionException, 'No registration allowed' unless Settings.regs_allow == true
    end
end
