class RankController < ApplicationController
  before_filter :login_required
  before_filter :admin_only
  layout 'admin'

  def index
    @ranks = Rank.find :all, :order => 'min_posts'
  end

end
