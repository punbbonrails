class SearchController < ApplicationController
  before_filter :can_search?
  before_filter :can_show_new?, :only => :show_new
  helper :forum

  def index
    @forums = Forum.get_forums(current_user.group)
  end

  def search
    params[:forum] = nil unless params[:forum] and params[:forum].to_i >= 1
    if params.key?(:author) and !params[:author].blank?
      users = User.get_id_by_username_pattern(params[:author])
      posts_by_author = Post.get_id_by_posters(users)
    end
    if params.key?(:keywords) and !params[:keywords].blank?
      if %w(all message).include? params[:search_in]
        posts_by_message = Post.find_id_by_contents(params[:keywords]) 
        posts_by_message.map! {|x| x[:id].to_i}
      end
      if %w(all topic).include? params[:search_in]
        topics = Topic.find_id_by_contents(params[:keywords])
        topics.map! {|x| x[:id].to_i}
      end
    end
    if posts_by_message.nil? or posts_by_author.nil?
      posts = posts_by_author || posts_by_message
    else
      posts = posts_by_author & posts_by_message
    end
    cu = current_user
    if params[:show_as] == 'posts'
      results = Post.get_id_by_posts_and_topics(posts, topics, cu, params[:forum])
    else
      results = Topic.get_id_by_posts_and_topics(posts, topics, cu, params[:forum])
    end
    SearchCache.prune
    result = {
      :results => results,
      :num_hits => results.length,
      :sort_by => params[:sort_by],
      :sort_dir => params[:sort_dir] || 'ASC',
      :show_as => params[:show_as] || 'topics'
    }
    sc = SearchCache.new
    sc.ident = get_ident
    sc.search_data = result
    sc.save!
    redirect_to :action => 'show', :id => sc.id
  end

  def show
    sc = SearchCache.find params[:id]
    r = sc.search_data
    render_results(r)
  end

  def user_posts
    cu = current_user
    params[:id] = cu.id if params[:id].nil?
    results = Topic.get_id_by_user(params[:id],cu)
    render_results(results)
  end

  def show_new
    cu = current_user
    results = Topic.get_id_by_time(cu, cu.last_visit)
    render_results(results)
  end

  def show_24h
    results = Topic.get_id_by_time(current_user, ApplicationController.now - 1.day)
    render_results(results)
  end

  def markread
  end

  def show_unanswered
    results = Topic.get_id_with_no_reply(current_user)
    render_results(results)
  end

  protected

    def can_search?
      cu = current_user
      raise PermissionException, 'You are not allowed to search' unless cu != nil and cu.group != nil and cu.group.g_search == true
    end

    def can_show_new?
      cu = current_user
      raise PermissionException, 'Guests can show new posts' if cu != nil and cu.is_guest?
    end

    def render_results(r)
      unless r.is_a? Hash
        r = {
          :results => r,
          :num_hits => r.length,
          :sort_by => params[:sort_by],
          :sort_dir => params[:sort_dir] || 'ASC',
          :show_as => params[:show_as] || 'topics'
        }
      end
      if r[:sort_by] == 'poster'
        order_field = r[:show_as] == 'topics' ? 'topics.poster' : 'posts.poster'
      elsif r[:sort_by] == 'subject'
        order_field = 'topics.subject'
      elsif r[:sort_by] == 'forum'
        order_field = 'topics.forum_id'
      elsif r[:sort_by] == 'last_post'
        order_field = 'topics.last_post'
      else
        order_field = r[:show_as] == 'posts' ? 'posts.created_at' : 'topics.created_at'
        group_by = ', topics.posted' if r[:show_as] == 'topics'
      end
      cu = current_user
      @pages = Paginator.new self, r[:num_hits], (r[:show_as] == 'posts' ? cu.get_disp_posts : cu.get_disp_topics), params[:page]
      p = {:order => order_field + ' ' + r[:sort_dir], :offset => @pages.current_page.to_sql[1], :limit => @pages.current_page.to_sql[0]}
      if r[:show_as] == 'posts'
	p[:include] = :topic
        @posts = Post.find r[:results], p
        render :action => 'posts'
      else
	p[:include] = :forum
        @topics = Topic.find r[:results], p
        render :action => 'topics'
      end
    end

end
