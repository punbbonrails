# Filters added to this controller will be run for all controllers in the application.
# Likewise, all the methods added will be available for all controllers.
class ApplicationController < ActionController::Base
  include AuthenticatedSystem
  before_filter :current_user 
  before_filter :can_read_board?
  before_filter :online_check

  protected

    def ApplicationController.now
      ActiveRecord::Base.default_timezone == :utc ? Time.now.utc : Time.now
    end

    def admin_only
      cu = current_user
      raise PermissionException, 'Only administrators can access this area' unless cu.is_a?(User) and cu.is_admin?
    end

    def can_moderate
      cu = current_user
      raise PermissionException, 'Only moderators can access this area' unless cu.is_a?(User) and cu.can_moderate?
    end

    def get_ident
      cu = current_user
      if cu.nil? or cu.is_guest?
        ident = request.env['REMOTE_ADDR']
        ident = 'Unknown' if ident.nil?
      else
        ident = cu.username
      end
      ident
    end

  private

    def can_read_board?
      cu = current_user
      ban = Ban.ban_by_ip? request.env['REMOTE_ADDR']
      raise PermissionException, "This IP has been banned: #{ban.message}" if ban != nil
      return true if cu != nil and cu.is_admin?
      ban = Ban.ban_by_username? cu.username if cu != nil
      raise PermissionException, "This user has been banned: #{ban.message}" if cu != nil and ban != nil
      return true if cu != nil and cu.group.g_read_board == true
      raise PermissionException
    end

    def online_check
      cu = current_user
      if cu.is_guest?
        ident = get_ident
        ticket = Online.find :first, :conditions => ['ident = ?', ident]
        if ticket.nil?
          ticket = Online.new
          ticket.ident = ident
        end
      else
        ticket = Online.find :first, :conditions => ['user_id = ?',cu.id]
        if ticket.nil?
          ticket = Online.new
          ticket.ident = get_ident
          ticket.user = cu
        end
        ticket.idle = false
      end
      now = ApplicationController.now
      ticket.logged = now
      ticket.save or flash[:error] = ticket.errors

      # This should be run as a background service instead
      Online.check(now)
      # This too
      Ban.prune(now)
    end

    def paginate_association(object, assoc_name, find_options = {}, per_page = 10)
      page = (params[:page] || 1).to_i

      item_count = object.send(assoc_name.to_s + '_count')
      offset = (page-1)*per_page

      @items = object.send(assoc_name).find(:all, {:offset => offset, :limit => per_page}.merge(find_options))
      @item_pages = Paginator.new self, item_count, per_page, page

      return @item_pages, @items
    end

    def rescue_action(e)
      if e.is_a?(ActiveRecord::RecordNotFound)
        flash[:error] = e.message unless e.message.blank?
        render "misc/bad_request", :status => 400
        return 
      end
      if e.is_a?(ActiveRecord::RecordInvalid)
        flash[:error] = e.record.errors
        render "misc/bad_request", :status => 400
        return
      end
      if e.is_a?(ParameterInvalid)
        flash[:error] = e.message unless e.message.blank?
        render "misc/bad_request", :status => 400
        return
      end
      if e.is_a?(PermissionException)
        flash[:error] = e.message unless e.message.blank?
        render "misc/access_denied", :status => 400
        return
      end
      super
    end
end

class PermissionException < StandardError; end
class ParameterInvalid < StandardError; end
