class ReportController < ApplicationController
  layout 'admin'
  before_filter :login_required
  before_filter :can_moderate
  def index
    @reports = Report.find :all, :include => [:topic, :forum, :reporter], :conditions => 'reports.zapped is null', :order => 'reports.created_at'
    @zapped_reports = Report.find :all, :include => [:topic, :forum, :reporter, :zapper], :conditions => 'reports.zapped IS NOT NULL', :order => 'reports.zapped DESC', :limit => 10
  end
end
