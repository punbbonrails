class ForumAdminController < ApplicationController
  before_filter :login_required
  before_filter :admin_only
  before_filter :find_forum_and_stuff, :only => [:edit, :update]
  before_filter :find_forum, :only => [:delete, :destroy]
  layout 'admin'

  def index
    @categories = Category.find :all, :order => :disp_position
    @forums = Forum.find :all, :include => :category, :order => "categories.disp_position, forums.disp_position"
  end

  def update_index
    create and return if params.key? 'add_forum'

    forums = Forum.find(params[:position].keys)
    for i in 0...forums.length
      next if params[:position][forums[i].id.to_s].nil?
      forums[i].disp_position = params[:position][forums[i].id.to_s]
      forums[i].save!
    end
    flash[:notice] = 'Categories updated'
    redirect_to :action => 'index'
  end

  def update
    if params.key? 'revert_perms'
      @forum.forum_perms.clear
      redirect_to :action => 'index'
      return
    end
    @forum.attributes = params[:forum]
    @forum.save!
    params[:forum_perms] = {} unless params.key? :forum_perms
    for g in @groups
      next if g.is_admin?
      fp = params[:forum_perms][g.id.to_s] || {}
      f = @forum.forum_perms.find :first, :conditions => "group_id=#{g.id}"
      fp[:read_forum] = fp[:read_forum] != nil
      fp[:post_replies] = fp[:post_replies] != nil
      fp[:post_topics] = fp[:post_topics] != nil
      if fp[:read_forum] == g.g_read_board and fp[:post_replies] == g.g_post_replies and fp[:post_topics] == g.g_post_topics
        f.destroy unless f.nil?
      else
        if f.nil?
          f = @forum.forum_perms.build
          f.group = g
        end
        f.attributes = fp
        f.save!
      end
    end
    flash[:notice] = 'Forum updated'
    redirect_to :action => 'index'
  rescue ActiveRecord::RecordInvalid
    flash[:error] = @forum.errors
    render :action => 'edit'
  end

  def create
    @forum = Forum.new params[:forum]
    @forum.save!
    flash[:notice] = 'Forum created'
    redirect_to :action => 'index'
  end

  def destroy
    @forum.destroy
    flash[:notice] = 'Forum deleted'
    redirect_to :action => 'index'
  end

  protected

    def find_forum_and_stuff
      @forum = Forum.find params[:id], :include => :forum_perms
      @categories = Category.find :all, :order => 'disp_position'
      @groups = Group.find :all
    end

    def find_forum
      @forum = Forum.find params[:id]
    end

end
