class BanController < ApplicationController
  before_filter :login_required
  before_filter :find_ban, :except => [:index, :new, :create]
  before_filter :can_ban?, :only => [:create, :update]
  layout 'admin'

  def new
    create and return unless params[:ban].is_a?(Hash) and params[:ban][:username].is_a?(String) and params[:ban][:username].blank?
    @ban = Ban.new params[:ban]
  end

  def create
    @ban = Ban.new params[:ban]
    @ban.save!
    flash[:notice] = 'Ban added'
    redirect_to :action => 'index'
  rescue ActiveRecord::RecordInvalid
    flash[:error] = @ban.errors
    render :action => 'new'
  end

  def update
    @ban.attributes = params[:ban]
    @ban.save!
    flash[:notice] = 'Ban updated'
    redirect_to :action => 'index'
  rescue ActiveRecord::RecordInvalid
    flash[:error] = @ban.errors
    render :action => 'edit'
  end

  def destroy
    @ban.destroy
    flash[:notice] = 'Ban deleted'
    redirect_to :action => 'index'
  end

  def index
    @bans = Ban.find :all, :order => 'id'
  end

  protected

    def can_ban?
      cu = current_user
      return true if cu.is_admin? or (Settings.mod_ban_users == true and cu.can_moderate?)
      raise PermissionException
    end

    def find_ban
      @ban = Ban.find params[:id]
    end
end
