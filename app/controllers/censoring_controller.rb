class CensoringController < ApplicationController
  layout 'admin'
  before_filter :login_required
  before_filter :can_moderate

  def index
    @censorings = Censoring.find :all, :order => 'id'
    flash[:error] = 'Unable to fetch censor word list' if @censorings.nil?
  end
end
