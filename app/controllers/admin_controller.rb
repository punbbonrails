class AdminController < ApplicationController
  before_filter :login_required
  before_filter :admin_only, :only => [:options, :update_options, :permissions, :update_permissions, :maintenance, :prune]
  before_filter :can_moderate

  def index
    begin
      @load_avg = File.read('/proc/loadavg')
    rescue
      begin
        @load_avg = %x(uptime)
      rescue
        @load_avg = 'Unavailable'
      end
    end
    begin
      @uname = %x(uname -a)
    rescue
      @uname = 'No uname info'
    end
  end

  def update_options
    form = params[:form]
    if form[:board_title] == ''
      flash[:error] = 'You must enter a board title'
      return
    end
    form[:admin_email].downcase! unless form[:admin_email].nil?
    form[:webmaster_email].downcase! unless form[:webmaster_email].nil?
    # check valid email
    form[:mailing_list] = form[:mailing_list].downcase.gsub(/[\s]/,'') unless form[:mailing_list].nil?
    form[:base_url] = form[:base_url][0..-2] if form.key? :base_url and form[:base_url][-1] == '/'
    form[:avatars_dir] = form[:avatars_dir][0..-2] if form.key? :avatars_dir and form[:avatars_dir][-1] == '/'
    # pun_linebreaks with additional_navlinks and announcement_message
    if form[:announcement] == ''
      form[:announcement_message] = 'Enter your announcement here.' 
      form[:announcement] = false
    end
    if form[:rules_message] == ''
      form[:rules_message] = 'Enter your rules here.'
      form[:rules] = false
    end
    if form[:maintenance_message] == ''
      form[:maintenance_message] = "The forums are temporarily down for maintenance. Please try again in a few minutes.\n\n/Administrator"
      form[:maintenance] = false
    end
    for i in [:timeout_visit, :timeout_online, :redirect_delay, :topic_review, :disp_topics_default, :disp_posts_default, :indent_num_spaces, :avatars_width, :avatars_height, :avatars_size]
      form[i] = form[i].to_i
    end
    for i in [:show_version, :show_user_info, :show_post_count, :smilies, :smilies_sig, :make_links, :quickpost, :users_online, :censoring, :ranks, :show_dot, :quickjump, :gzip, :search_all_forums, :report_method, :regs_report, :avatars, :subscriptions, :regs_allow, :regs_verify, :rules, :announcement, :maintenance]
      form[i] = form[i] == '1' or form[i] == true
    end
    if form[:timeout_online] > form[:timeout_visit]
      flash[:error] = 'The value of "Timeout online" must be smaller than the value of "Timeout visit".'
      return
    end
    for key in form.keys
      unless Settings[key].nil? or Settings[key] == form[key]
        Settings[key] = form[key]
      end
    end
    flash[:notice] = 'Options updated'
    render :action => 'options'
  end

  def update_permissions
    form = params[:form]
    for key in form.keys
      unless Settings[key].nil? or Settings[key] == form[key]
        Settings[key] = form[key]
      end
    end
    flash[:notice] = 'Permissions updated'
    render :action => 'permissions'
  end

  def find_user
    @users = User.find :all, :include => :group, :conditions => ['users.id > 1'], :order => 'username'
  end

  def ip_stats
    @max_posted = Post.maximum(:created_at, :conditions => ['poster_id=?',params[:id]], :group => 'poster_ip', :order => 'max_created_at DESC')
    @count_posts = Post.count(:conditions => ['poster_id=?',params[:id]], :group => 'poster_ip', :order => 'count_all DESC')
  end

  def prune
    # req_prune_days must be positive
    @forums = Forum.find :all, :include => :category, :conditions => 'forums.redirect_url IS NULL', :order => 'categories.disp_position, categories.id, forums.disp_position'
  end

  def users
    @groups = Group.find :all, :conditions => ['id != ?',Settings.group_ids[:pun_guest]], :order => 'g_title'
  end

  def show_users
    @posts = Post.find :all, :distinct, :include => :poster_user, :conditions => ['poster_ip = ? AND users.id > 1',params[:ip]], :order => 'poster DESC'
  end

end
