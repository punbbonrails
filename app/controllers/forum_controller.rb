class ForumController < ApplicationController
  before_filter :find_forum, :except => :index
  before_filter :can_view?, :except => :index

  def index
    cu = current_user
    @forums = cu.is_admin? ? Forum.get_all_forums : Forum.get_forums(cu.group)
  end

  def view
    redirect_to @forum.redirect_url and return unless @forum.redirect_url.blank?
    @topic_pages, @topics = paginate_association(@forum, :topics, { :order => "sticky DESC, #{@forum.sort_by == true ? 'created_at' : 'last_post'} DESC"}, current_user.get_disp_topics)
  end

  protected

    def find_forum
      @forum = Forum.find params[:id]
    end

    def can_view?
      raise PermissionException unless @forum.can_view? current_user
    end
end
