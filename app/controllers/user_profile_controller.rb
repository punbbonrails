class UserProfileController < ApplicationController
  PROFILE_ACTIONS = [:essentials, :personality, :admin, :messaging, :personal, :appearance, :privacy, :change_password, :update_password, :change_email, :update_email, :update_group]
  before_filter :login_required
  before_filter :find_user
  before_filter :can_moderator_change_password?, :only => [:update_password, :change_password]
  before_filter :admin_only, :only => :delete
  before_filter :can_delete?, :only => :delete
  before_filter :can_admin?, :only => :admin
  before_filter :admin_only, :only => :update_group

  def update_index
    @user.attributes= params[:form]
    if current_user.can_moderate?
      @user.email = params[:req_email]
      @user.num_posts = params[:num_posts]
      @user.admin_note = params[:admin_note]
    end
    @user.save ? flash[:notice] = 'User profile updated' : flash[:error] = @user.errors
    render :action => 'index'
  end

  def update_personality
    @user.attributes = params[:form]
    @user.save ? flash[:notice] = 'User profile updated' : flash[:error] = @user.errors
    render :action => 'personality'
  end

  def admin
    redirect_to :controller => 'ban', :action => 'create', 'ban[username]' => @user.username and return if params.key? :ban
    @groups = Group.find :all, :conditions => ["id != ?", Settings.group_ids[:pun_guest]], :order => "g_title"
    redirect_to params.merge({:action => 'update_group'}) and return if params.key? :update_group_membership
    if current_user.is_admin? and @user.can_moderate?
      @forums = Forum.find :all, :include => :category, :order => "categories.disp_position, categories.id, forums.disp_position", :conditions => "forums.redirect_url IS NULL"
      if params.key? :update_forums
        params[:moderator_in] = [] unless params[:moderator_in].is_a? Array
        params[:moderator_in].map! {|x| x.to_i}
        for f in 0...@forums.length
	  @forums[f].moderators = {} unless @forums[f].moderators.is_a? Hash
          if params[:moderator_in].include? @forums[f].id
            @forums[f].moderators[@user.id] = @user.username
          else
            @forums[f].moderators.delete @user.id
          end
          @forums[f].save!
        end
        flash[:notice] = 'Forums updated'
      end
    end
    render :action => 'delete_user_confirm' and return if params.key? :delete_user
  end

  def update_group
    group = Group.find params[:group_id]
    Forum.remove_moderator(@user) if @user.group.is_mod? and !group.is_mod?
    @user.group = group
    @user.save!
    flash[:notice] = 'User group updated'
    redirect_to :action => 'admin', :id => params[:id]
  end

  def delete
    @user = User.find params[:id], :include => :group
    if params.key? :delete_posts
      for post in @user.posts.find(:all, :include => :topic)
        post.topic.posts.find_first == post ? post.topic.destroy : post.destroy
      end
    end
    Forum.remove_moderator(@user) if @user.group.is_mod?
    @user.destroy
    flash[:notice] = 'User deleted'
    redirect_to :controller => 'admin'
  end

  def update_messaging
    @user.attributes = params[:form]
    @user.save ? flash[:notice] = 'User profile updated' : flash[:error] = @user.errors
    render :action => 'messaging'
  end

  def update_personal
    @user.attributes = params[:form]
    @user.title = params[:form][:title] if @user.group.g_set_title == true
    @user.save ? flash[:notice] = 'User profile updated' : flash[:error] = @user.errors
    render :action => 'personal'
  end

  def update_appearance
    @user.style = params[:form][:style]
    @user.show_smilies = params[:form].key? :show_smilies
    @user.show_sig = params[:form].key? :show_sig
    @user.show_avatars = params[:form].key? :show_avatars
    @user.show_img = params[:form].key? :show_img
    @user.show_img_sig = params[:form].key? :show_img_sig
    @user.disp_topics = params[:form].key?(:disp_topics) ? params[:form][:disp_topics] : nil
    @user.disp_posts = params[:form].key?(:disp_posts) ? params[:form][:disp_posts] : nil
    @user.url = params[:form][:url]
    @user.save ? flash[:notice] = 'User profile updated' : flash[:error] = @user.errors
    render :action => 'appearance'
  end

  def update_privacy
    @user.email_setting = params[:form][:email_setting].to_i
    @user.notify_with_post = params[:form].key? :notify_with_post
    @user.save_pass = params[:form].key? :save_pass
    @user.save ? flash[:notice] = 'User profile updated' : flash[:error] = @user.errors
    render :action => 'privacy'
  end

  def update_password
    flash[:error] = 'Old password does not match' and return unless current_user.can_moderate? or @user.authenticated?(params[:old_password])
    @user.passwd = params[:form][:passwd]
    @user.passwd_confirmation = params[:form][:passwd_confirmation]
    if @user.save
      flash[:notice] = 'User profile updated'
      redirect_to :action => 'index'
    else
      flash[:error] = @user.errors
      render :action => 'change_password'
    end
  end

  def update_email
    flash[:error] = 'Password does not match' and return unless @user.authenticated?(params[:req_password])
    @user.email = params[:req_new_email]
    if @user.save
      flash[:notice] = 'User profile updated'
      redirect_to :action => 'index'
    else
      flash[:error] = @user.errors
      render :action => 'change_email'
    end
  end

  protected

    def find_user
      cu = current_user
      @user = params.key?(:id) ? User.find_by_id(params[:id]) : cu
      return true if @user.is_a?(User) and !@user.is_guest? and (@user.id == cu.id or (cu.can_moderate? and Settings.mod_edit_users == true))
      raise PermissionException, 'Only moderators can access to other user profiles'
    end

    def can_update_group?
      return true if current_user.id != params[:id].to_i
      raise PermissionException, 'No permission to update user group'
    end

    def can_admin?
      return true if current_user.can_moderate?
      raise PermissionException, 'Only administrators and moderators can access this user administration area'
    end

    def can_delete?
      raise PermissionException, 'Administrators can not be deleted' if @user.is_admin?
    end

    def can_moderator_change_password?
      cu = current_user
      return true if @user.is_a?(User) and @user.id == cu.id
      return true if cu.can_moderate? and Settings.mod_change_passwords == true
      raise PermissionException, 'Moderators can not change user passwords'
    end
end
