class CategoryController < ApplicationController
  before_filter :login_required
  before_filter :admin_only
  layout 'admin'

  def index
    mapping = {'add_cat' => 'create', 'del_cat' => 'delete', 'update' => 'update'}
    for k in mapping.keys
      next unless params.key? k
      send(mapping[k])
      return
    end
    @categories = Category.find :all, :order => 'disp_position'
  end

  def delete
    @category = Category.find params[:cat_to_delete]
    @categories = Category.find :all, :order => 'disp_position'
    render :action => 'delete' # it's a hack because it's called from index, not as an action
  end

  def create
    cat = Category.new params[:category]
    cat.save!
    flash[:notice] = "Category #{cat.cat_name} added"
    redirect_to :action => 'index'
  end

  def destroy
    category = Category.find params[:id]
    category.destroy
    flash[:notice] = 'Category deleted'
    redirect_to :action => 'index'
  end

  def update
    categories = Category.find(params[:cat].keys)
    for i  in 0...categories.length
      categories[i].attributes = params[:cat][categories[i].id.to_s]
      categories[i].save!
    end
    flash[:notice] = 'Categories updated'
    redirect_to :action => 'index'
  end

end
