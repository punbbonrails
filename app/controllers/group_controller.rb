class GroupController < ApplicationController
  before_filter :login_required
  before_filter :admin_only
  before_filter :find_group, :only => [:edit, :update, :destroy, :delete]
  before_filter :can_destroy?, :only => [:delete, :destroy]
  layout 'admin'

  def index
    @groups = Group.find :all, :conditions => ['id > ?',Settings.group_ids[:pun_guest]], :order => 'g_title'
    @all_groups = Group.find :all, :order => 'id'
  end

  def update_index
    if params.key? :set_default_group
      raise ParameterInvalid unless params[:default_group].to_i > Settings.group_ids[:pun_guest]
      g = Group.find params[:default_group]
      Settings.default_user_group = g.id
      flash[:notice] = 'Default group updated'
    end
    if params.key? :add_group
      raise ParameterInvalid unless params[:base_group].to_i > Settings.group_ids[:pun_guest]
      g = Group.find params[:base_group]
      gg = g.clone
      gg.g_title += ' (new)'
      gg.save!
      flash[:notice] = 'New group added'
    end
    redirect_to :action => 'index'
  end

  def delete
    @groups = Group.find :all, :conditions => ['id != ? AND id != ?',Settings.group_ids[:pun_guest],@group.id], :order => 'g_title'
  end

  def destroy
    @dest_group = Group.find params[:move_to_group]
    @group = Group.find params[:id]
    for u in @group.members.find_all()
      @dest_group.members << u
      u.save!
    end
    @group.destroy
    flash[:notice] = 'Group deleted'
    redirect_to :action => 'index'
  end

  def update
    @group = Group.find params[:id]
    @group.attributes = params[:group]
    if @group.is_admin?
      keys = []
    elsif @group.is_guest?
      keys = [:g_read_board, :g_post_replies, :g_post_topics, :g_post_polls, :g_search, :g_search_users]
    else
      keys = [:g_read_board, :g_post_replies, :g_post_topics, :g_post_polls, :g_edit_posts, :g_delete_posts, :g_delete_topics, :g_set_title, :g_search, :g_search_users, :g_edit_subjects_interval, :g_post_flood, :g_search_flood]
    end
    for key in keys
      @group.send(key.to_s+'=',params[:group][key]) unless params[:group][key].nil?
    end
    @group.save!
    redirect_to :action => 'index'
  end

  protected

    def find_group
      @group = Group.find params[:id]
    end

    def can_destroy?
      raise PermissionException,'You can not delete default group' if params[:id].to_i == Settings.default_user_group
    end
end
