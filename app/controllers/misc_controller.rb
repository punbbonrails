class MiscController < ApplicationController
  skip_before_filter :can_read_board?
end
