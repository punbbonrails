require 'redcloth'

class Post < ActiveRecord::Base
  belongs_to :poster_user, :class_name => "User", :foreign_key => "poster_id"
  belongs_to :topic
  acts_as_ferret :fields => :message

  validates_length_of :message, :in => 1..65535
  validates_presence_of :topic # is it the rightful way?
  validates_associated :topic
  validates_length_of :poster, :maximum => 200, :allow_nil => true
  validates_length_of :poster_ip, :maximum => 15, :allow_nil => true
  validates_length_of :poster_email, :maximum => 50, :allow_nil => true
  validates_length_of :edited_by, :maximum => 200, :allow_nil => true

  validates_presence_of :poster_user, :on => :create
  before_validation_on_create :update_poster
  after_save :topic_update

  attr_accessible :message, :hide_smilies

  def message=(text)
    text.strip!
    if Settings.message_all_caps == false and text.upcase == text and User.current_user.group.id > Settings.group_ids[:pun_mod]
      # TODO: should be capitalize_words
      text = text.downcase.capitalize
    end
    write_attribute(:message,text)
  end

  SMILEYS = {
    ':)' => 'smile.png',
    '=)' => 'smile.png',
    ':|' => 'neutral.png',
    '=|' => 'neutral.png',
    ':(' => 'sad.png',
    '=(' => 'sad.png',
    ':D' => 'big_smile.png',
    '=D' => 'big_smile.png',
    ':o' => 'yikes.png',
    ':O' => 'yikes.png',
    ';)' => 'wink.png',
    ':/' => 'hmm.png',
    ':P' => 'tongue.png',
    ':lol:' => 'lol.png',
    ':mad:' => 'mad.png',
    ':rolleyes:' => 'roll.png',
    ':cool:' => 'cool.png'
  }
  def html_message
    # TODO: censoring here
    text = message
    if Settings.smilies == true and User.current_user.show_smilies == true and self.hide_smilies == false
      if Settings.message_bbcode == true
	for i in SMILEYS.keys
          text.gsub!(/(\W|^)#{Regexp.escape(i)}(?=.\W|\W.|\W$|$)/) do |match|
            $1+%|!/img/smilies/#{SMILEYS[match]}!|
          end
	end
      else
	for i in SMILEYS.keys
          text.gsub!(/(\W|^)#{Regexp.escape(i)}(?=.\W|\W.|\W$|$)/) do |match|
            $1+%|<img src="/img/smilies/#{SMILEYS[match]}"/>|
          end
	end
      end
    end
    if Settings.message_bbcode == true
      text = RedCloth.new(text).to_html
    else
      text = ERB::Util::html_escape(text).gsub("\n",'<br />')
    end
    text
  end

  def can_delete?(user)
    return true if self.topic != nil and self.topic.forum != nil and user.can_moderate_forum? self.topic.forum
    return true if user.group.g_delete_posts == true and (self.topic != nil and self.id != self.topic.posts.find_first.id or user.group.g_delete_topics == true) and self.poster_id == user.id and self.topic.closed == false
    false
  end

  def can_edit?(user)
    return true if self.topic != nil and self.topic.forum != nil and user.can_moderate_forum? self.topic.forum
    return true if user.group.g_edit_posts == true and self.poster_id == user.id and self.topic.closed == false
    false
  end

  def self.get_id_by_posters(posters)
    ActiveRecord::Base.connection.select_values("SELECT DISTINCT id FROM posts WHERE poster_id IN(#{posters.map{|x| quote(x)}.join(',')})").map{|x| x.to_i}
  end

  def self.get_id_by_posts_and_topics(posts,topics,user,forum_id = nil)
    p1 = ActiveRecord::Base.connection.select_values("SELECT DISTINCT p.id FROM posts AS p INNER JOIN topics AS t ON t.id=p.topic_id INNER JOIN forums AS f ON f.id=t.forum_id LEFT JOIN forum_perms AS fp ON (fp.forum_id=f.id AND fp.group_id=#{user.group_id}) WHERE (fp.read_forum IS NULL OR fp.read_forum=#{quote(true)}) AND p.id IN(#{posts.map{|x| quote(x)}.join(',')}) #{'AND t.forum_id='+quote(forum_id) unless forum_id.nil?}").map{|x| x.to_i} if posts.is_a? Array
    p2 = ActiveRecord::Base.connection.select_values("SELECT DISTINCT MIN(p.id) FROM posts AS p INNER JOIN topics AS t ON t.id=p.topic_id INNER JOIN forums AS f ON f.id=t.forum_id LEFT JOIN forum_perms AS fp ON (fp.forum_id=f.id AND fp.group_id=#{user.group_id}) WHERE (fp.read_forum IS NULL OR fp.read_forum=#{quote(true)}) AND t.id in (#{topics.map{|x| quote(x)}.join(',')}) #{'AND t.forum_id='+quote(forum_id) unless forum_id.nil?} GROUP BY t.id").map{|x| x.to_i} if topics.is_a? Array
    p1.nil? || p2.nil? ? (p1 || p2) : (p1 + p2).uniq
  end

  private

  def topic_update
    self.topic.update_on_post(self)
    self.topic.save
    unless self.poster_user.is_guest?
      self.poster_user.update_on_post(self)
      self.poster_user.save
    end
  end

  def update_poster
    self.poster = self.poster_user.username
    self.poster_email = self.poster_user.email unless self.poster_user.is_guest?
  end
end
