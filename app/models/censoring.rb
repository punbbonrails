class Censoring < ActiveRecord::Base
  validates_length_of :search_for, :within => 1..60
  validates_length_of :replace_with, :maximum => 60

  def self.censor_words(text)
    text
  end
end
