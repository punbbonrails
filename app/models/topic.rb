class Topic < ActiveRecord::Base
# belongs_to :last_post, :class_name => "Post", :foreign_key => "last_post_id"
  belongs_to :forum
  has_many :posts, :dependent => :destroy, :order => 'created_at'
  acts_as_ferret :fields => :subject

  validates_presence_of :forum
  validates_length_of :subject, :in => 1..70
  validates_associated :forum
  validates_length_of :poster, :maximum => 200, :allow_nil => true
  validates_length_of :last_poster, :maximum => 200, :allow_nil => true

  after_save :forum_update

  attr_accessible :subject

  def subject=(text)
    text.strip!
    if Settings.subject_all_caps == false and text.upcase == text and User.current_user.group.id > Settings.group_ids[:pun_mod]
      # TODO: should be capitalize_words
      text = text.downcase.capitalize
    end
    write_attribute(:subject,text)
  end

  def update_on_post(post)
    raise ArgumentError if post.topic_id != self.id
    self.num_replies = self.posts.length - 1
    self.last_post = post.updated_at
    self.last_post_id = post.id
    self.last_poster = post.poster
  end

  def can_reply?(user)
    return false unless self.forum.redirect_url.blank?
    return true if user.can_moderate_forum? self.forum
    return false if self.closed == true
    fp = self.forum.forum_perms.find_by_group_id user.group.id
    user.group.g_post_replies == true and (fp.nil? or fp.post_replies == true)
  end

  def get_post_position(post)
    ids = ActiveRecord::Base.connection.select_values("SELECT DISTINCT id FROM posts WHERE topic_id=#{self.id} ORDER BY created_at")
    ids.map {|i| i.to_i}.index(post.id)
  end

  def set_close_status(topics,closed)
    update_all "closed=#{quote(closed)}", "id in (#{topics.map{|x| quote(x)}.join(',')})"
  end

  def self.get_id_by_user(id,user)
    ActiveRecord::Base.connection.select_values("SELECT DISTINCT t.id FROM topics AS t INNER JOIN posts AS p ON t.id=p.topic_id INNER JOIN forums AS f ON f.id=t.forum_id LEFT JOIN forum_perms AS fp ON (fp.forum_id=f.id AND fp.group_id=#{user.group_id}) WHERE (fp.read_forum IS NULL OR fp.read_forum=#{quote(true)}) AND p.poster_id=#{user.id} GROUP BY t.id").map{|x| x.to_i}
  end

  def self.get_id_by_time(user,now)
    ActiveRecord::Base.connection.select_values("SELECT DISTINCT t.id FROM topics AS t INNER JOIN forums AS f ON f.id=t.forum_id LEFT JOIN forum_perms AS fp ON (fp.forum_id=f.id AND fp.group_id=#{user.group_id}) WHERE (fp.read_forum IS NULL OR fp.read_forum=#{quote(true)}) AND t.last_post>#{quote(now)}").map{|x| x.to_i}
  end

  def self.get_id_by_posts_and_topics(posts, topics, user, forum_id = nil)
    p = ActiveRecord::Base.connection.select_values("SELECT DISTINCT t.id FROM posts AS p INNER JOIN topics AS t ON t.id=p.topic_id INNER JOIN forums AS f ON f.id=t.forum_id LEFT JOIN forum_perms AS fp ON (fp.forum_id=f.id AND fp.group_id=#{user.group_id}) WHERE (fp.read_forum IS NULL OR fp.read_forum=#{quote(true)}) AND p.id IN(#{posts.map{|x| quote(x)}.join(',')}) #{'AND t.forum_id='+quote(forum_id) unless forum_id.nil?} GROUP BY t.id").map{|x| x.to_i} unless posts.nil?
    p.nil? || topics.nil? ? (p || topics) : (p+topics).uniq
  end

  def self.get_id_with_no_reply(user)
    ActiveRecord::Base.connection.select_values("SELECT DISTINCT t.id FROM topics AS t INNER JOIN forums AS f ON f.id=t.forum_id LEFT JOIN forum_perms AS fp ON (fp.forum_id=f.id AND fp.group_id=#{user.group_id}) WHERE (fp.read_forum IS NULL OR fp.read_forum=#{quote(true)}) AND t.num_replies=0 AND t.moved_to IS NULL").map{|x| x.to_i}
  end

  private

    def forum_update
      self.forum.update_on_topic
      self.forum.save
    end
end
