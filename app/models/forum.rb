class Forum < ActiveRecord::Base
  has_many :topics, :dependent => :destroy
  serialize :moderators
  belongs_to :category, :foreign_key => "cat_id"
  has_many :forum_perms, :dependent => :destroy
  has_many :groups, :through => :forum_perms
  serialize :moderators

  validates_presence_of :category
  validates_associated :category

  validates_length_of :forum_name, :within => 1..80
  validates_length_of :redirect_url, :maximum => 100, :allow_nil => true
  validates_length_of :last_poster, :maximum => 200, :allow_nil => true
  validates_numericality_of :disp_position, :only_integer => true

  attr_accessible :forum_name, :forum_desc, :redirect_url, :sort_by, :disp_position, :cat_id

  def update_on_topic
    last_topic = Topic.find(:first, :conditions => "forum_id=#{self.id} AND moved_to IS NULL", :order => "last_post DESC")

    if last_topic.nil?
      self.num_topics = 0
      self.num_posts = 0
      self.last_post = nil
      self.last_post_id = nil
      self.last_poster = nil
    else
      conditions = "moved_to IS NULL AND forum_id=#{self.id}"
      # TODO: howto merge two queries into one as in punbb?
      self.num_topics = Topic.count(conditions)
      self.num_posts = self.num_topics + Topic.sum(:num_replies, :conditions => conditions)
      self.last_post = last_topic.last_post
      self.last_post_id = last_topic.last_post_id
      self.last_poster = last_topic.last_poster
    end
  end

  def can_post?(user)
    return false unless self.redirect_url.blank?
    return true if user.can_moderate_forum? self
    fp = self.forum_perms.find_by_group_id user.group.id
    user.group.g_post_topics == true and (fp.nil? or fp.post_topics == true)
  end

  def can_view?(user)
    return true if user.can_moderate_forum? self
    fp = self.forum_perms.find_by_group_id user.group.id
    fp.nil? or fp.read_forum == true
  end

  def self.remove_moderator(user)
    forums = Forum.find :all
    for f in 0...forums.length
      next unless forums[f].moderators.is_a?(Hash) and forums[f].moderators.key? user.id
      forums[f].moderators.delete user.id 
      forums[f].save!
    end
  end

  def self.get_all_forums
    Forum.find :all, :include => :category, :order => 'categories.disp_position, forums.disp_position'
  end

  def self.get_forums(group)
    Forum.find :all, :include => :category, :joins => "LEFT JOIN forum_perms ON forum_perms.forum_id=forums.id AND forum_perms.group_id = #{group.id}", :conditions => "(forum_perms.read_forum IS NULL OR forum_perms.read_forum=#{quote(true)})", :order => 'categories.disp_position, forums.disp_position'
  end
end
