class Category < ActiveRecord::Base
  has_many :forums, :foreign_key => "cat_id", :dependent => :destroy

  validates_presence_of :cat_name
  validates_length_of :cat_name, :within => 1..80
  validates_numericality_of :disp_position, :only_integer => true
end
