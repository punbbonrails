class Ban < ActiveRecord::Base
  validates_length_of :username, :maximum => 200, :allow_nil => true
  validates_length_of :email, :maximum => 50, :allow_nil => true
  validates_length_of :ip, :maximum => 256, :allow_nil => true
  validates_format_of :ip, :with => /^(?:(?:\d{1,3}\.){0,3}\d{1,3} ?)*$/
  validate :can_ban?

  def self.prune(now)
    Ban.delete_all ['expire <= ?', now]
  end

  def self.ban_by_username?(username)
    Ban.find_by_username(username)
  end

  # email pattern is not supported yet
  def self.ban_by_email?(email)
    bans = Ban.find_all
    for ban in bans
      next if ban.email.nil? or ban.email.blank?
      return ban if email.rindex(ban.email) == email.length-ban.email.length
    end
    nil
  end

  def self.ban_by_ip?(ip)
    bans = Ban.find_all
    for ban in bans
      next if ban.ip.nil? or ban.ip.blank?
      banned_ip = ban.ip.strip.split(/ +/)
      for bip in banned_ip
        return ban if (ip+'.').index(bip+'.') == 0
      end
    end
    nil
  end

  protected
    def can_ban?
      unless self.username.nil? or self.username.blank?
        u = User.find_by_username self.username, :include => :group
	@errors.add :username, 'User does not exist' and return if u.nil?
        @errors.add :username, 'Can not ban administrators' if u.group.is_admin?
      end
      @errors.add :username, "and email or ip must not be empty altogether" if self.username.blank? and self.email.blank? and self.ip.blank?
    end
end
