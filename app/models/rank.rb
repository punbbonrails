class Rank < ActiveRecord::Base
  validates_length_of :rank, :within => 1..50
  validates_numericality_of :min_posts, :only_integer => true
  validates_each :min_posts do |r,a,v|
    r.errors.add a, 'must be positive integer' unless v >= 0
  end
  validates_uniqueness_of :min_posts
end
