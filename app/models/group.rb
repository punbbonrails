class Group < ActiveRecord::Base
  has_many :members, :class_name => 'User'
  has_many :forum_perms
  has_many :forums, :through => :forum_perms

  validates_length_of :g_title, :within => 1..50
  validates_length_of :g_user_title, :maximum => 50, :allow_nil => true
  validates_uniqueness_of :g_title, :case_sensitive => false

  attr_accessible :g_title, :g_user_title

  def is_admin?
    self.id == Settings.group_ids[:pun_admin]
  end

  def is_mod?
    self.id == Settings.group_ids[:pun_mod]
  end

  def is_guest?
    self.id == Settings.group_ids[:pun_guest]
  end
end
