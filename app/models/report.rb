class Report < ActiveRecord::Base
  belongs_to :topic
  belongs_to :post
  belongs_to :forum
  belongs_to :reporter, :class_name => 'User', :foreign_key => 'reporter_id'
  belongs_to :zapper, :class_name => 'User', :foreign_key => 'zapper_id'

  validates_presence_of :reporter
  validates_length_of :message, :within => 1..65535
end
