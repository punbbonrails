class User < ActiveRecord::Base
  belongs_to :group
  has_one :online
  has_many :posts, :foreign_key => 'poster_id'

  @@current_user = nil
  @@guest = nil

  validates_presence_of :username
  validates_length_of :username, :within => 3..40

  validates_presence_of :passwd, :if => :password_required?
  validates_length_of :passwd, :within => 5..40, :if => :password_required?

  validates_presence_of :email
  validates_length_of :email,   :within => 3..100
  validates_format_of :email, :with => /^([^@\s]+)@((?:[-a-z0-9]+.)+[a-z]{2,})$/

  validates_length_of :title, :maximum => 50, :allow_nil => true
  validates_length_of :realname, :maximum => 40, :allow_nil => true
  validates_length_of :url, :maximum => 100, :allow_nil => true
  validates_length_of :jabber, :maximum => 75, :allow_nil => true
  validates_length_of :icq, :maximum => 12, :allow_nil => true
  validates_length_of :msn, :maximum => 50, :allow_nil => true
  validates_length_of :yahoo, :maximum => 30, :allow_nil => true
  validates_length_of :location, :maximum => 30, :allow_nil => true
  validates_length_of :language, :maximum => 25
  validates_length_of :style, :maximum => 25
  validates_length_of :registration_ip, :maximum => 15, :allow_nil => true
  validates_length_of :admin_note, :maximum => 30, :allow_nil => true
  validates_length_of :activate_string, :maximum => 50, :allow_nil => true
  validates_length_of :activate_key, :maximum => 50, :allow_nil => true
  validates_length_of :signature, :maximum => 400, :allow_nil => true
  validates_each :signature do |r,a,v|
    r.errors.add "Maximum 4 lines" if v != nil and v.count("\n") > 3
  end
  validates_numericality_of :disp_posts, :only_integer => true, :allow_nil => true
  validates_numericality_of :disp_topics, :only_integer => true, :allow_nil => true

  validates_presence_of :passwd_confirmation, :if => :password_required?
  validates_confirmation_of :passwd, :if => :password_required?
  validates_presence_of :email_confirmation, :on => :create
  validates_confirmation_of :email, :on => :create
  before_save :encrypt_password

  validates_uniqueness_of :username, :email, :case_sensitive => false
  attr_accessor :passwd
  attr_accessible :realname, :url, :jabber, :icq, :msn, :aim, :yahoo, :location, :use_avatar, :signature, :disp_topics, :disp_posts, :email_setting, :save_pass, :notify_with_post, :show_smilies, :show_img, :show_img_sig, :show_avatars, :show_sig, :timezone, :language, :style, :passwd, :passwd_confirmation, :email, :email_confirmation

  # Authenticates a user by their login name and unencrypted password.  Returns the user or nil.
  def self.authenticate(username, passwd)
    u = find_by_username(username) # need to get the salt
    u && u.authenticated?(passwd) ? u : nil
  end

  # Encrypts some data with the salt.
  def self.encrypt(passwd, salt)
    Digest::SHA1.hexdigest("--#{salt}--#{passwd}--")
  end

  # Encrypts the password with the user salt
  def encrypt(passwd)
    self.class.encrypt(passwd, salt)
  end

  def authenticated?(passwd)
    password == encrypt(passwd)
  end

  def update_on_post(post)
    raise ArgumentError unless post != nil && post.poster_id == self.id
    self.num_posts = self.num_posts + 1
    self.last_post = post.updated_at
  end

  def get_title
    return 'Banned' if false
    return self.title if self.title != nil and self.title != ''
    return group.g_user_title if group.g_user_title and group.g_user_title != ''
    return 'Guest' if group.id == Settings.group_ids[:pun_guest]
    return 'Ranked' if false
    return 'Member'
  end

  def User.current_user
    @@current_user
  end

  def User.current_user=(user)
    @@current_user = user
  end

  def User.guest
    @@guest ||= User.find_by_username 'Guest'
  end

  def is_guest?
    self.id == User.guest.id
    # self.id == 1
  end

  def is_mod?
    self.group_id == Settings.group_ids[:pun_mod]
  end

  def is_admin?
    self.group_id == Settings.group_ids[:pun_admin]
  end

  def can_moderate?
    self.group_id < Settings.group_ids[:pun_guest]
  end

  def is_forum_mod?(forum)
    is_mod? and forum.moderators.is_a?(Hash) and forum.moderators.key?(self.id)
  end

  def can_moderate_forum?(forum)
    is_admin? or is_forum_mod?(forum)
  end

  def is_flood?(now)
    self.last_post != nil and (now - self.last_post) <= self.group.g_post_flood
  end

  def get_disp_topics 
    self.disp_topics ? self.disp_topics : Settings.disp_topics_default
  end

  def get_disp_posts 
    self.disp_posts ? self.disp_posts : Settings.disp_posts_default
  end

  def self.get_id_by_username_pattern(username)
    username.gsub!(/\*/,'%')
    return [] if username == 'Guest'
    ActiveRecord::Base.connection.select_values("SELECT id FROM users WHERE username LIKE #{quote(username)}").map{|x| x.to_i}
  end

  protected
  # before filter 
  def encrypt_password
    return if passwd.blank?
    self.salt = Digest::SHA1.hexdigest("--#{Time.now.to_s}--#{username}--") if new_record?
    self.password = encrypt(passwd)
  end

  def password_required?
    password.blank? || !passwd.blank?
  end
end
