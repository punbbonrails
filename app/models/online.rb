class Online < ActiveRecord::Base
  belongs_to :user

  def Online.logout(user)
    raise ArgumentError, 'user is not a User', caller unless user.is_a? User
    # user_id == 1 => guest, don't delete
    Online.delete_all "user_id=#{user.id}" unless user.id == 1
  end

  def Online.login(user,ident)
    raise ArgumentError, 'ident is not a string', caller unless ident.is_a? String

    Online.delete_all ['user_id=1 AND ident=?',ident] unless ident.blank?
    # don't need to create a new record, ApplicationController#online_check will
    # take care of that
  end

  # The logic: users will be kept online in timeout_online seconds
  # If they don't access the forum after timeout_online seconds, they will
  # be marked as idle and not shown in online list.
  # If they are guests, their records will be removed.
  # If they are registered users, their records still remain until
  # timeout_visit past
  def Online.check(now)
    online_timepoint = now - Settings.timeout_online
    visit_timepoint = now - Settings.timeout_visit

    # Remove timed-out guests
    Online.delete_all ['logged < ? AND user_id=1',online_timepoint]

    # this block could be rewritten as several sql queries
    # update onlines set idle=true when logged < online_timepoint and logged > visit_timepoint
    # update users set users.last_visit=onlines.logged where logged < visit_timepoint and users.id=onlines.user_id and onlines.user_id != 1
    # delete from onlines where logged < visit_timepoint
    tickets = Online.find :all, :conditions => ['logged < ?',online_timepoint]
    for ticket in tickets
      next if ticket.user.id == 1
      if ticket.logged < visit_timepoint
        ticket.user.last_visit = ticket.logged
        ticket.destroy
      else
        ticket.idle = true
        ticket.save
      end
    end
  end
end
