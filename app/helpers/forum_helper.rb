module ForumHelper
  def topic_info(topic)
    cu = current_user
    is_guest = cu.is_guest?
    icon_text = 'Normal icon'
    item_status = ''
    icon_type = 'icon'
    subject = link_to(h(topic.subject), :controller => 'topic', :action => 'view', :id => topic.id)
    subject += ' '
    subject += content_tag('span', "by&nbsp;#{h(topic.poster)}", :class => 'byuser')
    if topic.moved_to != nil
      last_post = '&nbsp;'
      subject = 'Moved ' + subject
    else
      last_post = link_to(topic.last_post, :controller => 'topic', :action => 'view', :pid => topic.last_post_id, :anchor => 'p'+topic.last_post_id.to_s)
      last_post += ' '
      last_post += content_tag('span', "by &nbsp; #{h(topic.last_poster)}", :class => 'byuser')
      if topic.closed == true
        icon_text = 'Closed icon'
	item_status = 'iclosed'
      end
    end

    subject_new_posts = ''
    if !is_guest and topic.last_post > cu.last_visit and topic.moved_to.nil?
      icon_text += ' ' + 'New icon'
      item_status += ' inew'
      icon_type = 'icon inew'
      subject = '<strong>'+subject+'</strong>'
      subject_new_posts = '<span class="newtext">[&nbsp;'+link_to('New posts', {:controller => 'topic', :action => 'view', :id => topic.id, :new_posts => true}, {:title => 'New posts info'})+'&nbsp;]</span>'
    end

    if !is_guest and Settings.show_dot == true
      subject = (topic.posts.find_by_poster_id(cu.id) != nil ? '<strong>&middot;</strong>&nbsp;' : '&nbsp;&nbsp;') + subject
    end

    paginator = ActionController::Pagination::Paginator.new self, topic.num_replies+1, cu.get_disp_posts

    if topic.sticky == true
      subject = '<span class="stickytext">Sticky: </span>'+subject
      item_status += ' isticky'
      icon_text += ' Sticky'
    end

    subject += '&nbsp; ' + subject_new_posts unless subject_new_posts.blank?
    subject += ' ' + pagination_links(paginator, :params => {:controller => 'topic', :action => 'view', :id => topic.id}) if paginator.page_count > 1
    return icon_text, item_status, icon_type, subject, last_post
  end
end
