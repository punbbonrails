module UserHelper
   def hu(a)
      a.nil? || a.blank? ? "Unknown" : h(a)
   end
end
