# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  def timezone_options(tz = nil)
    timezones = {
      -12 => '-12',
      -11 => '-11',
      -10 => '-10',
      -9.5 => '-9.5',
      -9 => '-09',
      -8.5 => '-8.5',
      -8 => '-08 PST',
      -7 => '-07 MST',
      -6 => '-06 CST',
      -5 => '-05 EST',
      -4 => '-04 AST',
      -3.5 => '-3.5',
      -3 => '-03 ADT',
      -2 => '-02',
      -1 => '-01',
      0 => '00 GMT',
      1 => '+01 CET',
      2 => '+02',
      3 => '+03',
      3.5 => '+03.5',
      4 => '+04',
      4.5 => '+04.5',
      5 => '+05',
      5.5 => '+05.5',
      6 => '+06',
      6.5 => '+06.5',
      7 => '+07',
      8 => '+08',
      9 => '+09',
      9.5 => '+09.5',
      10 => '+10',
      10.5 => '+10.5',
      11 => '+11',
      11.5 => '+11.5',
      12 => '+12',
      13 => '+13',
      14 => '+14'
    }
    text = ''
    for i in timezones.keys.sort
      if tz != nil and tz == i
        text += "<option value=\"#{i}\" selected=\"selected\">#{timezones[i]}</option>"
      else
        text += "<option value=\"#{i}\">#{timezones[i]}</option>"
      end
    end
    text
  end
  
  def languages
    {'en' => 'English', 'vi' => 'Vietnamese'}
  end

  def language_options(lang = nil)
    text = ''
    for i in languages.keys.sort
      if lang == i
        text += "<option value=\"#{i}\" selected=\"selected\">#{languages[i]}</option>"
      else
        text += "<option value=\"#{i}\">#{languages[i]}</option>"
      end
    end
    text
  end

  def styles
    Dir["#{RAILS_ROOT}/public/style/*.css"].map{|x| File.basename(x).gsub(/\.css$/,'')}
  end

  def style_options(style = nil)
    text = ''
    for i in styles.sort
      if style == i
        text += "<option value=\"#{i}\" selected=\"selected\">#{i}</option>"
      else
        text += "<option value=\"#{i}\">#{i}</option>"
      end
    end
    text
  end
end
