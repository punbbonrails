desc 'Dump a table in yaml format'
task :dump_fixture => :environment do |task|
  table_name = ENV['TABLE']
  ActiveRecord::Base.establish_connection(RAILS_ENV.to_sym)
  i = 0
  puts ActiveRecord::Base.connection.select_all("SELECT * FROM #{table_name}").inject({}) { |hash, record|
    hash["#{table_name}_#{i += 1}"] = record
    hash
  }.to_yaml
end
