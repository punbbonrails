class CreateReports < ActiveRecord::Migration
  def self.up
    create_table :reports do |t|
      t.column :post_id, :integer
      t.column :topic_id, :integer
      t.column :forum_id, :integer
      t.column :reporter_id, :integer, :null => false
      t.column :created_at, :timestamp
      t.column :message, :text, :null => false
      t.column :zapped, :timestamp
      t.column :zapper_id, :integer
    end
  end

  def self.down
    drop_table :reports
  end
end
