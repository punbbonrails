class CreateCategories < ActiveRecord::Migration
  def self.up
    create_table :categories do |t|
      t.column "cat_name", :string, :limit => 80, :default => "New Category", :null => false
      t.column "disp_position", :integer, :limit => 10, :default => 0, :null => false
    end
  end

  def self.down
    drop_table :categories
  end
end
