class CreateOnlines < ActiveRecord::Migration
  def self.up
    create_table :onlines do |t|
      t.column :user_id, :integer, :default => 1
      t.column :ident, :string, :default => ''
      t.column :logged, :timestamp
      t.column :idle, :boolean, :default => false
    end
    add_index :onlines, [:user_id]
  end

  def self.down
    drop_table :onlines
  end
end
