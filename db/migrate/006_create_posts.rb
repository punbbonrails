class CreatePosts < ActiveRecord::Migration
  def self.up
    create_table :posts do |t|
      t.column "poster", :string, :limit => 200, :default => "", :null => false
      t.column "poster_id", :integer, :limit => 10, :default => 1, :null => false
      t.column "poster_ip", :string, :limit => 15
      t.column "poster_email", :string, :limit => 50
      t.column "message", :text, :default => "", :null => false
      t.column "hide_smilies", :boolean, :default => false, :null => false
      t.column "edited_by", :string, :limit => 200
      t.column "topic_id", :integer, :limit => 10, :default => 0, :null => false
      t.column "created_at", :timestamp
      t.column "updated_at", :timestamp
    end
    add_index :posts, [:topic_id]
    add_index :posts, [:poster_id, :topic_id]
  end

  def self.down
    drop_table :posts
  end
end
