class CreateRanks < ActiveRecord::Migration
  def self.up
    create_table :ranks do |t|
      t.column :rank, :string, :limit => 50, :default => ''
      t.column :min_posts, :integer, :default => 0
    end
  end

  def self.down
    drop_table :ranks
  end
end
