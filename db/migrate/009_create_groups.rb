class CreateGroups < ActiveRecord::Migration
  def self.up
    create_table :groups do |t|
      t.column "g_title", :string, :limit => 50, :default => "", :null => false
      t.column "g_user_title", :string, :limit => 50
      t.column "g_read_board", :boolean, :default => true, :null => false
      t.column "g_post_replies", :boolean, :default => true, :null => false
      t.column "g_post_topics", :boolean, :default => true, :null => false
      t.column "g_post_polls", :boolean, :default => true, :null => false
      t.column "g_edit_posts", :boolean, :default => true, :null => false
      t.column "g_delete_posts", :boolean, :default => true, :null => false
      t.column "g_delete_topics", :boolean, :default => true, :null => false
      t.column "g_set_title", :boolean, :default => true, :null => false
      t.column "g_search", :boolean, :default => true, :null => false
      t.column "g_search_users", :boolean, :default => true, :null => false
      t.column "g_edit_subjects_interval", :integer, :limit => 6, :default => 300, :null => false
      t.column "g_post_flood", :integer, :limit => 6, :default => 30, :null => false
      t.column "g_search_flood", :integer, :limit => 6, :default => 30, :null => false
    end

    say_with_time "Adding default groups ..." do
      path = "db/migrate/groups.yaml"
      records = YAML::load(File.open(File.expand_path(path, RAILS_ROOT)))
      records.each do |record|
        group = Group.new(record[1])
        group.id = record[1]['id']
        group.save
      end
    end
  end

  def self.down
    drop_table :groups
  end
end
