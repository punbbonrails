class CreateTopics < ActiveRecord::Migration
  def self.up
    create_table :topics do |t|
      t.column "poster", :string, :limit => 200, :default => "", :null => false
      t.column "subject", :string, :default => "", :null => false
      t.column "created_at", :timestamp
      t.column "last_post", :timestamp
      t.column "last_post_id", :integer, :limit => 10, :default => 0, :null => false
      t.column "last_poster", :string, :limit => 200
      t.column "num_views", :integer, :limit => 8, :default => 0, :null => false
      t.column "num_replies", :integer, :limit => 8, :default => 0, :null => false
      t.column "closed", :boolean, :default => false, :null => false
      t.column "sticky", :boolean, :default => false, :null => false
      t.column "moved_to", :integer, :limit => 10
      t.column "forum_id", :integer, :limit => 10, :default => 0, :null => false
    end
    add_index :topics, [:forum_id]
    add_index :topics, [:moved_to]
  end

  def self.down
    drop_table :topics
  end
end
