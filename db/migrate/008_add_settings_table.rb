class AddSettingsTable < ActiveRecord::Migration
  def self.up
    create_table :settings, :force => true do |t|
      t.column :var, :string, :null => false
      t.column :value, :string, :null => true
      t.column :created_at, :datetime
      t.column :updated_at, :datetime
    end

    say_with_time "Adding default settings ..." do
      Settings.cur_version = 'GIT'
      Settings.board_title = 'My PunBB on Rails forum'
      Settings.board_desc = 'Unfortunately no one can be told what PunBB on Rails is - you have to see it for yourself.'
      Settings.server_timezone = 0
      Settings.time_format = 'H:i:s'
      Settings.date_format = 'Y-m-d'
      Settings.timeout_visit = 600
      Settings.timeout_online = 300
      Settings.redirect_delay = 1
      Settings.show_version = false
      Settings.show_user_info = true
      Settings.show_post_count = true
      Settings.smilies = true
      Settings.smilies_sig = true
      Settings.make_links = true
      Settings.default_lang = 'English'
      Settings.default_style = 'Oxygen'
      Settings.default_user_group = 4
      Settings.topic_review = 15
      Settings.disp_topics_default = 30
      Settings.disp_posts_default = 25
      Settings.indent_num_spaces = 4
      Settings.quickpost = true
      Settings.users_online = true
      Settings.censoring = false
      Settings.ranks = true
      Settings.show_dot = true
      Settings.quickjump = true
      Settings.gzip = false
      Settings.additional_navlinks = ''
      Settings.report_method = 0
      Settings.regs_report = 0
      Settings.mailing_list = ''
      Settings.avatars = true
      Settings.avatars_dir = 'img/avatars'
      Settings.avatars_width = 60
      Settings.avatars_height = 60
      Settings.avatars_size = 10240
      Settings.search_all_forums = true
      Settings.base_url = ''
      Settings.admin_email = ''
      Settings.webmaster_email = ''
      Settings.subscriptions = true
      Settings.regs_allow = true
      Settings.regs_verify = false
      Settings.announcement = false
      Settings.annoucement_message = 'Enter your annoucement here'
      Settings.rules = false
      Settings.rules_message = 'Enter your rules here'
      Settings.maintenance = false
      Settings.maintenance_message = 'The forums are temporarily down for maintenance. Please try again in a few minutes.<br />\n<br />\nAdministrator'
      Settings.mod_edit_users = true
      Settings.mod_rename_users = false
      Settings.mod_change_passwords = false
      Settings.mod_ban_users = false
      Settings.message_bbcode = true
      Settings.message_img_tag = true
      Settings.message_all_caps = true
      Settings.subject_all_caps = true
      Settings.sig_all_caps = true
      Settings.sig_bbcode = true
      Settings.sig_img_tag = false
      Settings.sig_length = 400
      Settings.sig_lines = 4
      Settings.allow_banned_email = true
      Settings.allow_dupe_email = false
      Settings.force_guest_email = true

      Settings.group_ids = { 
          :pun_admin => 1,
          :pun_mod => 2,
          :pun_guest => 3,
          :pun_member => 4
        }
    end
  end

  def self.down
    drop_table :settings
  end
end
