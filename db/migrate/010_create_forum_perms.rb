class CreateForumPerms < ActiveRecord::Migration
  def self.up
    create_table :forum_perms do |t|
      # t.column :name, :string
      # TODO: the primary key should be (forum_id, group_id)
      t.column :forum_id, :integer, :null => false
      t.column :group_id, :integer, :null => false
      t.column :read_forum, :boolean, :null => false
      t.column :post_replies, :boolean, :null => false
      t.column :post_topics, :boolean, :null => false
    end
  end

  def self.down
    drop_table :forum_perms
  end
end
