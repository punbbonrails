class CreateSearchCaches < ActiveRecord::Migration
  def self.up
    create_table :search_caches do |t|
      t.column :ident, :string, :null => false, :limit => 200
      t.column :search_data, :text, :null => false
    end
  end

  def self.down
    drop_table :search_caches
  end
end
