class CreateForums < ActiveRecord::Migration
  def self.up
    create_table :forums do |t|
      t.column "forum_name", :string, :limit => 80, :default => "New forum", :null => false
      t.column "forum_desc", :text
      t.column "redirect_url", :string, :limit => 100
      t.column "moderators", :text
      t.column "num_topics", :integer, :limit => 8, :default => 0, :null => false
      t.column "num_posts", :integer, :limit => 8, :default => 0, :null => false
      t.column "last_post", :timestamp
      t.column "last_post_id", :integer, :limit => 10
      t.column "last_poster", :string, :limit => 200
      t.column "sort_by", :boolean, :default => false, :null => false
      t.column "disp_position", :integer, :limit => 10, :default => 0, :null => false
      t.column "cat_id", :integer, :limit => 10, :null => false
    end
  end

  def self.down
    drop_table :forums
  end
end
