class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
      t.column "group_id", :integer, :limit => 10, :default => 4, :null => false
      t.column "username", :string, :limit => 200, :default => "", :null => false
      t.column "password", :string, :limit => 40, :default => "", :null => false
      t.column "email", :string, :limit => 50, :default => "", :null => false
      t.column "title", :string, :limit => 50
      t.column "realname", :string, :limit => 40
      t.column "url", :string, :limit => 100
      t.column "jabber", :string, :limit => 75
      t.column "icq", :string, :limit => 12
      t.column "msn", :string, :limit => 50
      t.column "aim", :string, :limit => 30
      t.column "yahoo", :string, :limit => 30
      t.column "location", :string, :limit => 30
      t.column "use_avatar", :boolean, :default => false, :null => false
      t.column "signature", :text, :default => ''
      t.column "disp_topics", :integer, :limit => 3
      t.column "disp_posts", :integer, :limit => 3
      t.column "email_setting", :integer, :default => 1, :limit => 1, :null => false
      t.column "save_pass", :boolean, :default => true, :null => false
      t.column "notify_with_post", :boolean, :default => false, :null => false
      t.column "show_smilies", :boolean, :default => true, :null => false
      t.column "show_img", :boolean, :default => true, :null => false
      t.column "show_img_sig", :boolean, :default => true, :null => false
      t.column "show_avatars", :boolean, :default => true, :null => false
      t.column "show_sig", :boolean, :default => true, :null => false
      t.column "timezone", :float, :default => 0.0, :null => false
      t.column "language", :string, :limit => 25, :default => "English", :null => false
      t.column "style", :string, :limit => 25, :default => "Oxygen", :null => false
      t.column "num_posts", :integer, :limit => 10, :default => 0, :null => false
      t.column "last_post", :timestamp
      t.column "created_at", :timestamp
      t.column "registration_ip", :string, :limit => 15, :default => "0.0.0.0", :null => false
      t.column "last_visit", :timestamp
      t.column "admin_note", :string, :limit => 30
      t.column "activate_string", :string, :limit => 50
      t.column "activate_key", :string, :limit => 8
      t.column "salt", :string, :limit => 40
    end
    add_index :users, [:created_at]
    add_index :users, [:username]

    say_with_time "Adding default users ..." do
      path = "db/migrate/users.yaml"
      records = YAML::load(File.open(File.expand_path(path, RAILS_ROOT)))
      records.each do |record|
        user = User.new(record[1])
        # these fields are filtered out
        user.id = record[1]['id']
        user.username = record[1]["username"]
        user.group_id = record[1]["group_id"]
        user.passwd_confirmation = user.passwd
        user.email_confirmation = user.email
	user.created_at = Time.now.utc
        unless user.save
	  puts(user.errors.collect {|e,m| "#{e.humanize unless e == "base"} #{m}"})
	end
      end
    end
  end

  def self.down
    drop_table :users
  end
end
