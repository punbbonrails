class CreateCensorings < ActiveRecord::Migration
  def self.up
    create_table :censorings do |t|
      t.column "search_for", :string, :limit => 60, :default => "", :null => false
      t.column "replace_with", :string, :limit => 60, :default => "", :null => false
    end
  end

  def self.down
    drop_table :censorings
  end
end
