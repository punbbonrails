class CreateBans < ActiveRecord::Migration
  def self.up
    create_table :bans do |t|
      t.column "username", :string, :limit => 200
      t.column "ip", :string
      t.column "email", :string, :limit => 50
      t.column "message", :string
      t.column "expire", :datetime
    end
  end

  def self.down
    drop_table :bans
  end
end
