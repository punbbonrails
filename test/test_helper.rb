ENV["RAILS_ENV"] = "test"
require File.expand_path(File.dirname(__FILE__) + "/../config/environment")
require 'test_help'

class Test::Unit::TestCase
  self.use_transactional_fixtures = false
  self.use_instantiated_fixtures  = false
  
  def assert_form(options)
    assert_tag :tag => 'form', :attributes => {:action => @controller.url_for(options.merge({:only_path => true}))}
  end

  def assert_link_to(options)
    link = @controller.url_for(options.merge({:only_path => true}))
    p = {:tag => (options[:tag] || 'a'),:attributes => {}}
    p[:attributes][options[:tag_href] || 'href'] = link
    assert_tag p
  end
end
