require File.dirname(__FILE__) + '/../test_helper'

require 'user'
require 'post'
require 'topic'

class PostTest < Test::Unit::TestCase
  fixtures :posts, :users, :topics, :groups, :forums, :settings, :categories

  def test_poster_user
    assert_equal posts(:p1).poster_user.username, 'user one hundred and one'
    assert_equal posts(:p2).poster_user.username, 'user one hundred'
  end

  def test_topic
    assert_equal posts(:p1).topic.subject, 'topic 1'
    assert_equal posts(:p2).topic.subject, 'topic 1'
  end

  def test_save
    Settings.message_all_caps = false
    Settings.group_ids = {:pun_mod => 1}
    User.current_user = User.find_by_id 100
    p = Post.new
    p.message = 'test'
    p.topic = Topic.find_by_id 1
    p.poster_id = 101
    assert p.save

    p = Post.new
    p.message = 'test'
    assert p.save == false

    p = Post.new
    p.topic = Topic.find_by_id 1
    p.message = ''
    assert p.save == false

    p = Post.new
    p.topic = Topic.find_by_id 1
    p.message = 'A'*65536
    assert p.save == false
  end

  def test_message_assignment
    Settings.message_all_caps = true
    Settings.group_ids = {:pun_mod => 1}
    User.current_user = User.new
    User.current_user.group = Group.new
    User.current_user.group.id = 2

    p = Post.new

    p.message = '  message with surrounding spaces  '
    assert_equal p.message, 'message with surrounding spaces'

    p.message = '  UPPER CASE MESSAGE  '
    assert_equal p.message,'UPPER CASE MESSAGE'

    Settings.message_all_caps = false

    p.message = '  message with surrounding spaces  '
    assert_equal p.message, 'message with surrounding spaces'

    p.message = '  UPPER CASE MESSAGE  '
    assert_equal p.message,'Upper case message'

    User.current_user.group.id = 1

    p.message = '  message with surrounding spaces  '
    assert_equal p.message, 'message with surrounding spaces'

    p.message = '  UPPER CASE MESSAGE  '
    assert_equal p.message,'UPPER CASE MESSAGE'
  end

  def test_html_message
    Settings.smilies = false
    Settings.message_bbcode = false
    p = Post.new
    p.message = "<b>unfriendly</b> message\nwith newline"
    assert_equal p.html_message, '&lt;b&gt;unfriendly&lt;/b&gt; message<br />with newline'

    # ban test
    assert false, 'ban test not implemented'

    # smilies test
    assert false, 'smilies test not implemented'

    # redcloth test
    assert false, 'redcloth test not implemented'
  end

  def test_poster
    p = Post.new
    p.topic = topics(:f11)
    p.message = 'test'
    assert p.save == true
    p.poster = 'a'*200
    assert p.save == true
    p.poster = 'a'*201
    assert p.save == false
  end

  def test_poster_ip
    p = Post.new
    p.topic = topics(:f11)
    p.message = 'test'
    assert p.save == true
    p.poster_ip = 'a'*15
    assert p.save == true
    p.poster_ip = 'a'*16
    assert p.save == false
  end

  def test_poster_email
    p = Post.new
    p.topic = topics(:f11)
    p.message = 'test'
    assert p.save == true
    p.poster_email = 'a'*50
    assert p.save == true
    p.poster_email = 'a'*51
    assert p.save == false
  end

  def test_edited_by
    p = Post.new
    p.topic = topics(:f11)
    p.message = 'test'
    assert p.save == true
    p.edited_by = 'a'*200
    assert p.save == true
    p.edited_by = 'a'*201
    assert p.save == false
  end

  def test_can_delete?
    p = Post.new
    p.topic = topics(:f11)
    assert_equal false,p.can_delete?(users(:mod))
    p.topic.forum.moderators = {users(:mod).id => 'The mighty mod'}
    assert_equal true,p.can_delete?(users(:mod))
    assert_equal true,p.can_delete?(users(:mod))
    assert_equal true,p.can_delete?(users(:admin))
    p = posts(:p2)
    p.poster_user.group.g_delete_posts = true
    assert_equal true,p.can_delete?(p.poster_user)
    assert_equal false,p.can_delete?(users(:u101))
    p.poster_user.group.g_delete_posts = false
    assert_equal false,p.can_delete?(p.poster_user)

    p.topic.closed = true
    p.poster_user.group.g_delete_posts = true
    assert_equal false,p.can_delete?(p.poster_user)
    p.poster_user.group.g_delete_posts = false
    assert_equal false,p.can_delete?(p.poster_user)
    
    p = posts(:p1)
    p.poster_user.group.g_delete_topics = true
    p.poster_user.group.g_delete_posts = true
    assert_equal true,p.can_delete?(p.poster_user)
    assert_equal false,p.can_delete?(users(:u100))
    p.poster_user.group.g_delete_posts = false
    assert_equal false,p.can_delete?(p.poster_user)
    p.poster_user.group.g_delete_topics = false
    assert_equal false,p.can_delete?(p.poster_user)
    p.poster_user.group.g_delete_posts = false
    assert_equal false,p.can_delete?(p.poster_user)
  end

  def test_can_edit?
    p = Post.new
    p.topic = topics(:f11)
    assert_equal false,p.can_edit?(users(:mod))
    p.topic.forum.moderators = {users(:mod).id => 'The mighty mod'}
    assert_equal true,p.can_edit?(users(:mod))
    assert_equal true,p.can_edit?(users(:admin))
    p = posts(:p2)
    p.poster_user.group.g_edit_posts = true
    assert_equal true,p.can_edit?(p.poster_user)
    assert_equal false,p.can_edit?(users(:u101))
    p.poster_user.group.g_edit_posts = false
    assert_equal false,p.can_edit?(p.poster_user)

    p.topic.closed = true
    p.poster_user.group.g_edit_posts = true
    assert_equal false,p.can_edit?(p.poster_user)
    p.poster_user.group.g_edit_posts = false
    assert_equal false,p.can_edit?(p.poster_user)
  end
end
