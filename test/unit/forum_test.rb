require File.dirname(__FILE__) + '/../test_helper'

require 'forum'
require 'topic'
require 'category'

class ForumTest < Test::Unit::TestCase
  fixtures :forums, :topics, :categories, :settings, :users, :groups, :forum_perms

  def test_topics
    assert_equal forums(:f1).topics.length, 2
    assert_equal forums(:f2).topics.length, 2
    assert_equal forums(:f3).topics.length, 0
  end

  def test_category
    assert_equal forums(:f1).category.cat_name, 'category one'
    assert_equal forums(:f2).category.cat_name, 'category two'
    assert_equal forums(:f3).category.cat_name, 'category one'
  end

  def test_update_on_forum
    f = Forum.find_by_id 1
    f.update_on_topic
    assert_equal f.num_topics, 2
    assert_equal f.num_posts, 5
    assert_equal f.last_post, topics(:f12).last_post
    assert_equal f.last_post_id, 12
    assert_equal f.last_poster, 'f12'

    f = Forum.find_by_id 2
    f.update_on_topic
    assert_equal f.num_topics, 2
    assert_equal f.num_posts, 9
    assert_equal f.last_post, topics(:f22).last_post
    assert_equal f.last_post_id, 22
    assert_equal f.last_poster, 'f22'

    f = Forum.find_by_id 3
    f.update_on_topic
    assert_equal f.num_topics, 0
    assert_equal f.num_posts, 0
    assert_nil f.last_post
    assert_nil f.last_post_id
    assert_nil f.last_poster
  end

  def test_can_post?
    f = Forum.new
    f.category = categories(:cat1)
    f.forum_name = 'blah'
    f.redirect_url = 'blah'
    f.save!
    for u in [users(:guest), users(:mod), users(:admin)]
      assert_equal false,f.can_post?(u)
      uu = u.clone
      uu.group = groups(:can_post_topics)
      assert_equal false, f.can_post?(uu)
      uu.group = groups(:cant_post_topics)
      assert_equal false, f.can_post?(uu)
    end
    fp = ForumPerm.new
    fp.forum_id = f.id
    fp.group_id = users(:u100).group.id
    users(:u100).group.g_post_topics = true
    fp.read_forum = true
    fp.save!
    for url in [nil, '']
      f.redirect_url = url
      assert_equal true, f.can_post?(users(:admin))
      assert_equal true, f.can_post?(users(:mod))
      uu = users(:guest).clone
      uu.group = groups(:can_post_topics)
      assert_equal true, f.can_post?(uu)
      uu.group = groups(:cant_post_topics)
      assert_equal false, f.can_post?(uu)

      fp.post_topics = true
      fp.save!
      assert_equal true, f.can_post?(users(:u100))
      fp.post_topics = false
      fp.save!
      assert_equal false, f.can_post?(users(:u100))
    end
  end

  def test_can_view?
    f = forums(:f1)
    assert_equal true, f.can_view?(users(:u100))
    fp = ForumPerm.new
    fp.forum_id = f.id
    fp.group_id = users(:u100).group.id
    fp.read_forum = true
    fp.save!
    assert_equal true, f.can_view?(users(:u100))
    fp.read_forum = false
    fp.save!
    assert_equal false, f.can_view?(users(:u100))
  end

  def test_delete
    forums(:f1).destroy
    assert_raise(ActiveRecord::RecordNotFound) {Topic.find_by_id(topics(:f11).id)}
    assert_raise(ActiveRecord::RecordNotFound) { Topic.find_by_id(topics(:f12).id)}
    assert_not_nil Topic.find_by_id(topics(:f21).id)
    assert_not_nil Topic.find_by_id(topics(:f22).id)
  end

  def test_forum_name
    a = Forum.new
    a.category = categories(:cat1)
    a.forum_name = 'a'
    assert_equal true, a.save
    a.forum_name = 'a'*80
    assert_equal true, a.save
    a.forum_name = ''
    assert_equal false, a.save
    a.forum_name = 'a'*81
    assert_equal false, a.save
  end

  def test_redirect_url
    a = Forum.new
    a.category = categories(:cat1)
    assert_equal true, a.save
    a.redirect_url = 'a'
    assert_equal true, a.save
    a.redirect_url = 'a'*100
    assert_equal true, a.save
    a.redirect_url = ''
    assert_equal true, a.save
    a.redirect_url = 'a'*101
    assert_equal false, a.save
  end

  def test_last_poster
    a = Forum.new
    a.category = categories(:cat1)
    assert_equal true, a.save
    a.last_poster = 'a'
    assert_equal true, a.save
    a.last_poster = 'a'*200
    assert_equal true, a.save
    a.last_poster = ''
    assert_equal true, a.save
    a.last_poster = 'a'*201
    assert_equal false, a.save
  end

  def test_remove_moderator
    f = forums(:f1)
    f.moderators = {users(:u100).id => 'u100', users(:u101).id => 'u101'}
    f.save!
    Forum.remove_moderator(users(:u100))
    f.reload
    assert_not_nil f.moderators[users(:u101).id]
    assert_nil     f.moderators[users(:u100).id]
  end
end
