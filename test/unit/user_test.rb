require File.dirname(__FILE__) + '/../test_helper'

class UserTest < Test::Unit::TestCase
  fixtures :users, :groups, :onlines, :posts, :topics, :forums, :categories

  def test_group
    assert_equal users(:guest).group.g_title, 'Guest group'
    assert_equal users(:u100).group.g_title, 'group one'
    assert_equal users(:u101).group.g_title, 'group one'
  end

  def test_online
    # Don't test User#online with guest because it's a special case
    assert_equal users(:u100).online.ident, 'user one hundred'
    assert_equal users(:u101).online.ident, 'user one hundred and one'
  end

  def test_save
    # on_create

    # username out of range
    u = User.new
    u.username = 'tes'
    u.email = 'test@test.com'
    u.email_confirmation = 'test@test.com'
    u.passwd = 'tests'
    u.passwd_confirmation = 'tests'
    assert u.save == true
    u.destroy
    u = User.new
    u.username = 'te'
    u.email = 'test@test.com'
    u.email_confirmation = 'test@test.com'
    u.passwd = 'tests'
    u.passwd_confirmation = 'tests'
    assert u.save == false
    # username out of range
    u = User.new
    u.username = 'tes'
    u.email = 'test@test.com'
    u.email_confirmation = 'test@test.com'
    u.passwd = 'tests'
    u.passwd_confirmation = 'tests'
    assert u.save == true
    u.destroy
    u = User.new
    u.username = 'u'*41
    u.email = 'test@test.com'
    u.email_confirmation = 'test@test.com'
    u.passwd = 'tests'
    u.passwd_confirmation = 'tests'
    assert u.save == false
    # signature too long
    u = User.new
    u.username = 'tes'
    u.email = 'test@test.com'
    u.email_confirmation = 'test@test.com'
    u.passwd = 'tests'
    u.passwd_confirmation = 'tests'
    assert u.save == true
    u.destroy
    u = User.new
    u.username = 'tes'
    u.email = 'test@test.com'
    u.email_confirmation = 'test@test.com'
    u.passwd = 'tests'
    u.passwd_confirmation = 'tests'
    u.signature = 'a'*401
    assert u.save == false
    # signature has too many lines
    u = User.new
    u.username = 'tes'
    u.email = 'test@test.com'
    u.email_confirmation = 'test@test.com'
    u.passwd = 'tests'
    u.passwd_confirmation = 'tests'
    u.signature = "a\na\na\na\na"
    assert u.save == false

    # username in range
    u = User.new
    u.username = 'tes'
    u.email = 'test@test.com'
    u.email_confirmation = 'test@test.com'
    u.passwd = 'tests'
    u.passwd_confirmation = 'tests'
    assert u.save == true
    u.destroy
    # username in range
    u = User.new
    u.username = 'u'*40
    u.email = 'test@test.com'
    u.email_confirmation = 'test@test.com'
    u.passwd = 'tests'
    u.passwd_confirmation = 'tests'
    assert u.save == true
    
    # username uniqueness
    uu = User.new
    uu.username = 'tes'
    uu.email = 'test2@test.com'
    uu.email_confirmation = 'test@test.com'
    uu.passwd = 'tests'
    uu.passwd_confirmation = 'tests'
    assert uu.save == false
    # email uniqueness
    uu = User.new
    uu.username = 'tes2'
    uu.email = 'test@test.com'
    uu.email_confirmation = 'test@test.com'
    uu.passwd = 'tests'
    uu.passwd_confirmation = 'tests'
    assert uu.save == false

  end

  def test_self_authenticate
    u = User.new
    u.username = 'tes'
    u.email = 'test@test.com'
    u.email_confirmation = 'test@test.com'
    u.passwd = 'tests'
    u.passwd_confirmation = 'tests'
    assert u.save == true
    assert_not_nil User.authenticate('tes','tests')
    assert_nil User.authenticate('tes','test')
  end

  def test_self_encrypt
    assert_equal User.encrypt('passwd','salt'), "3804acaf5feed714b1b20cb080596e3b23ba62ef"
    assert_equal User.encrypt('passwd',''), "988afdb7a730f72230cd75553f39c2b63c93f499"
    assert_equal User.encrypt('','salt'), "16e2b3457ff3ba6622554a8563a7d0ad54bb5614"
    assert_equal User.encrypt('',''), "742ce30a73b59259a9b55e5eaf0e97e813167d60"
  end

  def test_encrypt
    assert false, 'User#encrypt not tested'
  end

  def test_authenticated?
    u = User.new
    u.username = 'tes'
    u.email = 'test@test.com'
    u.email_confirmation = 'test@test.com'
    u.passwd = 'tests'
    u.passwd_confirmation = 'tests'
    assert u.save == true
    assert u.authenticated?('tests') == true
    assert u.authenticated?('test') == false
  end

  def test_update_on_post
    p = Post.find_by_id 1
    u = User.find_by_id 100
    assert_raise(ArgumentError) {u.update_on_post(p)}
    User.current_user = User.find_by_id 100
    p = Post.find_by_id 3
    p.message = 'test'
    assert p.save == true # updated_at update
    u.update_on_post(p)
    assert_equal u.num_posts, 13
    assert_equal u.last_post, p.updated_at
  end

  def test_get_title
    assert false, 'User#get_title not tested'
  end

  def test_current_user
    u = User.find_by_id 1
    User.current_user = u
    assert_equal User.current_user.id, u.id
    User.current_user = nil
    assert_nil User.current_user
  end

  def test_guest
    u = User.find_by_id 1
    assert_equal User.guest.id, u.id
  end

  def test_is_guest?
    u = User.find_by_id 1
    assert u.is_guest? == true
    u = User.find_by_id 100
    assert u.is_guest? == false
  end

  def test_is_mod?
    Settings.group_ids = {:pun_mod => 3}
    u = User.find_by_id 1
    assert u.is_mod? == true
    u = User.find_by_id 100
    assert u.is_mod? == false
  end

  def test_is_admin?
    Settings.group_ids = {:pun_admin => 3}
    u = User.find_by_id 1
    assert u.is_admin? == true
    u = User.find_by_id 100
    assert u.is_admin? == false
  end

  def test_can_moderate?
    Settings.group_ids = {:pun_guest => 4}
    u = User.find_by_id 1
    assert u.can_moderate? == true
    u = User.find_by_id 100
    assert u.can_moderate? == false
  end

  def test_encrypt_password
    assert false, 'User#encrypt_password not tested'
  end

  def test_password_required?
    u = User.new
    class << u
      def check_password_required?
        password_required?
      end
    end
    assert u.check_password_required? == true
    u.password = 'blah'
    assert u.check_password_required? == false
    u.password = ''
    assert u.check_password_required? == true
    u.password = 'bala'
    u.passwd = ''
    assert u.check_password_required? == false
    u.password = 'bala'
    u.passwd = 'fass'
    assert u.check_password_required? == true
  end

  def make_user
    u = User.new
    u.username = 'maks_user'
    u.email = 'maksuser@test.com'
    u.email_confirmation = 'maksuser@test.com'
    u.passwd = 'tests'
    u.passwd_confirmation = 'tests'
    assert u.save == true
    u
  end

  def test_max_length
    tests = {:title => 50, :realname => 40, :url => 100, :jabber => 75, :icq => 12, :msn => 50, :yahoo => 30, :location => 30, :language => 25, :style => 25, :registration_ip => 15, :admin_note => 30, :activate_string => 50, :activate_key => 50, :signature => 400}
    tests.each do |k,v|
      u = make_user
      u.send(k.to_s+'=','a'*v)
      assert u.save == true
      u.destroy
      u = make_user
      u.send(k.to_s+'=','a'*(v+1))
      assert u.save == false
      u.destroy
    end
  end

  def test_posters
    assert_equal 2, users(:u100).posts.size
    assert_equal 0, users(:guest).posts.size
    assert_equal 1, users(:u101).posts.size
    assert_equal 0, users(:admin).posts.size
  end

  def test_is_flood?
    u = User.new
    u.group = Group.new
    u.group.g_post_flood = 20
    now = Time.now
    u.last_post = nil
    assert_equal false, u.is_flood?(now)
    assert_equal false, u.is_flood?(now+20)
    assert_equal false, u.is_flood?(now+21)
    u.last_post = now
    assert_equal true, u.is_flood?(now)
    assert_equal true, u.is_flood?(now+20)
    assert_equal false, u.is_flood?(now+21)
  end
end
