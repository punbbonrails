require File.dirname(__FILE__) + '/../test_helper'

class RankTest < Test::Unit::TestCase
  fixtures :ranks

  def test_rank
    r = Rank.new
    r.rank = 'a'
    r.min_posts = 1
    assert r.save == true
    r.rank = 'a'*50
    assert r.save == true
    r.rank = ''
    assert r.save == false
    r.rank = 'a'*51
    assert r.save == false
  end
end
