require File.dirname(__FILE__) + '/../test_helper'

class BanTest < Test::Unit::TestCase
  fixtures :bans, :users, :groups, :settings

  def test_can_ban?
    b = Ban.new
    assert_equal false, b.valid?
    b.username = 'testsadsa'
    assert_equal false, b.valid?
    assert_not_nil b.errors.on(:username)
    b.username = 'user one hundred'
    assert_equal true, b.valid?
    b.username = 'admin'
    assert_equal false, b.valid?
    assert_not_nil b.errors.on(:username)
    b.username = nil
    b.email = 'test@bla.com'
    assert_equal true, b.valid?
    b.email = nil
    b.ip = '123.123'
    assert_equal true, b.valid?
  end

  def test_prune
    now = Time.now
    b = Ban.new :username => 'user one hundred', :expire => now
    b.save!
    Ban.prune(now-1)
    assert_not_nil Ban.find_by_id(b.id)
    assert_not_nil bans(:u100)
    assert_not_nil bans(:another)
    Ban.prune(now)
    assert_nil Ban.find_by_id(b.id)
    assert_not_nil bans(:u100)
    assert_not_nil bans(:another)
  end

  def test_ban_by_username?
    assert_not_nil Ban.ban_by_username?('user one hundred and one')
    assert_nil     Ban.ban_by_username?('user one hundred')
  end

  def test_ban_by_email?
    assert_not_nil Ban.ban_by_email?('blah@yahoo.com')
    assert_not_nil Ban.ban_by_email?('blah@yyahoo.com')
    assert_nil     Ban.ban_by_email?('blah@yahoo.com.vn')
    assert_nil     Ban.ban_by_email?('bluh@gmail.com')
  end

  def test_ban_by_ip?
    assert_nil     Ban.ban_by_ip?('127.0.0.1')
    assert_not_nil Ban.ban_by_ip?('172.16.32.1')
    assert_nil     Ban.ban_by_ip?('172.161.32.1')
    assert_not_nil Ban.ban_by_ip?('172.16.32.12')
    assert_nil     Ban.ban_by_ip?('172.17.32.1')
    assert_not_nil Ban.ban_by_ip?('10.0.0.113')
    assert_not_nil Ban.ban_by_ip?('10.12.12.12')
    assert_nil     Ban.ban_by_ip?('10.12.12.121')
  end
end
