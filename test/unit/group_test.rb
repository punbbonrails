require File.dirname(__FILE__) + '/../test_helper'

require 'group'
require 'user'

class GroupTest < Test::Unit::TestCase
  fixtures :groups, :users, :settings

  def test_members
    assert_equal groups(:g100).members.length, 2
    assert_equal groups(:g101).members.length, 0
  end

  def test_g_title
    g = Group.new
    g.g_title = 'a'
    assert g.save == true
    g.g_title = 'a'*50
    assert g.save == true
    g.g_title = ''
    assert g.save == false
    g.g_title = nil
    assert g.save == false
    g.g_title = 'a'*51
    assert g.save == false
  end

  def test_g_user_title
    g = Group.new
    g.g_title = 'a'
    assert g.save == true
    g.g_user_title = 'a'*50
    assert g.save == true
    g.g_user_title = 'a'*51
    assert g.save == false
  end

  def test_is_admin?
    g = Group.new
    g.id = 1
    Settings.group_ids = {:pun_admin => 1}
    assert_equal true, g.is_admin?
    Settings.group_ids = {:pun_admin => 2}
    assert_equal false, g.is_admin?
  end

  def test_is_mod?
    g = Group.new
    g.id = 1
    Settings.group_ids = {:pun_mod => 1}
    assert g.is_mod? == true
    Settings.group_ids = {:pun_mod => 2}
    assert g.is_mod? == false
  end

  def test_is_guest?
    g = Group.new
    g.id = 1
    Settings.group_ids = {:pun_guest => 1}
    assert g.is_guest? == true
    Settings.group_ids = {:pun_guest => 2}
    assert g.is_guest? == false
  end
end
