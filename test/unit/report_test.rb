require File.dirname(__FILE__) + '/../test_helper'

class ReportTest < Test::Unit::TestCase
  fixtures :reports, :users, :topics, :posts, :forums

  def test_save
    r = Report.new
    assert r.save == false

    r.message = 'report message'
    assert r.save == false

    r.reporter = User.find_by_id 100
    r.message = ''
    assert r.save == false

    r.message = 'report message'
    assert r.save == true
  end

  def test_topic
    assert_equal reports(:r1).topic.subject, 'topic 1'
    assert_equal reports(:r2).topic.subject, 'topic 2'
  end

  def test_post
    r = reports(:r1)
    assert_equal r.post.topic_id, 1
    assert_equal r.post.poster_id, 101

    r = reports(:r2)
    assert_equal r.post.topic_id, 1
    assert_equal r.post.poster_id, 100
  end

  def test_forum
    assert_equal reports(:r1).forum.forum_name, 'Forum 1'
    assert_equal reports(:r2).forum.forum_name, 'Forum 2'
  end

  def test_reporter
    assert_equal reports(:r1).reporter.username, 'Guest'
    assert_equal reports(:r2).reporter.username, 'user one hundred'
  end

  def test_zapper
    assert_nil reports(:r1).zapper
    assert_equal reports(:r2).zapper.username, 'user one hundred and one'
  end
end
