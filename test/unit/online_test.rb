require File.dirname(__FILE__) + '/../test_helper'

require 'online'
require 'user'
require 'settings'

class OnlineTest < Test::Unit::TestCase
  fixtures :onlines, :users

  def test_user
    assert_equal onlines(:guest1).user.username, 'Guest'
    assert_equal onlines(:user1).user.username, 'user one hundred'
  end

  def test_logout
    # check for nil
    assert_raise(ArgumentError) { Online.logout(nil) }
    assert_equal Online.count, 5

    # check for invalid object
    assert_raise(ArgumentError) { Online.logout(OnlineTest.new) }
    assert_equal Online.count, 5

    # guest
    u = User.find_by_id 1
    Online.logout(u)
    assert_equal Online.count, 5

    # remove correctly
    u = User.find_by_id 100
    Online.logout(u)
    assert_equal Online.count, 4
    assert_nil Online.find_by_id(4)
  end

  def test_login
    assert_raise(ArgumentError) { Online.login(nil,nil) }
    assert_raise(ArgumentError) { Online.login(nil,Object.new) }

    assert_equal Online.count, 5

    Online.login(nil,'user one hundred')
    assert_equal Online.count, 5

    Online.login(nil,'172.16.32.1')
    assert_equal Online.count, 3
    assert_nil Online.find_by_id(1)
    assert_nil Online.find_by_id(2)
  end

  def test_online_check
    o = Online.find_by_id 1
    now = o.logged
    Settings.timeout_online = 30
    Settings.timeout_visit = 50

    # workaround because yaml date format is not suitable
    oo = Online.find :all
    oo.each {|o| o.save}

    Online.check(now)
    assert_equal Online.count, 5
    Online.check(now+10)
    assert_equal Online.count, 5
    Online.check(now+20)
    assert_equal Online.count, 5
    Online.check(now+30)
    assert_equal Online.count, 5

    # Guest 1 out at 31st second
    Online.check(now+31)
    assert_equal Online.count, 4
    assert_equal Online.count('user_id=1'), 2

    # guest 2 follows guest 1
    Online.check(now+32)
    assert_equal Online.count, 3
    assert_equal Online.count('user_id=1'), 1

    # so does guest 3
    Online.check(now+33)
    assert_equal Online.count, 2
    assert_equal Online.count('user_id=1'), 0
    #
    # nothing changes in 34th second
    Online.check(now+34)
    assert_equal Online.count, 2
    assert_equal Online.count('user_id=1'), 0

    # now user 4 is marked idle
    Online.check(now+35)
    assert_equal Online.count, 2
    assert Online.find_by_id(4).idle
    assert Online.find_by_id(5).idle == false

    # now user 5 is marked idle
    Online.check(now+36)
    assert_equal Online.count, 2
    assert Online.find_by_id(4).idle
    assert Online.find_by_id(5).idle

    # ok, nothing changes from now
    Online.check(now+54)
    assert_equal Online.count, 2
    assert Online.find_by_id(4).idle
    assert Online.find_by_id(5).idle

    # user 4 gets kicked out
    Online.check(now+55)
    assert_equal Online.count, 1

    # user 5 gets kicked out
    Online.check(now+56)
    assert_equal Online.count, 0
  end
end
