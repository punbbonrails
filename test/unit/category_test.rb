require File.dirname(__FILE__) + '/../test_helper'

require 'category'
require 'forum'

class CategoryTest < Test::Unit::TestCase
  fixtures :categories, :forums

  def test_forums
    assert_equal categories(:cat1).forums.length, 2
    assert_equal categories(:cat2).forums.length, 1
    assert_equal categories(:cat3).forums.length, 0
  end

  def test_delete
    categories(:cat1).destroy
    assert_raise(ActiveRecord::RecordNotFound) {Forum.find_by_id(forums(:f1).id)}
    assert_not_nil Forum.find_by_id(forums(:f2).id)
    assert_raise(ActiveRecord::RecordNotFound) { Forum.find_by_id(forums(:f3).id)}
  end

  def test_cat_name
    a = Category.new
    a.cat_name = 'a'
    assert a.save == true
    a.cat_name = 'a'*80
    assert a.save == true
    a.cat_name = ''
    assert a.save == false
    a.cat_name = 'A'*81
    assert a.save == false
  end
end
