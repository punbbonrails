require File.dirname(__FILE__) + '/../test_helper'

class CensoringTest < Test::Unit::TestCase
  fixtures :censorings

  def test_search_for
    a = Censoring.new
    a.search_for = 'a'
    assert a.save == true
    a.search_for = 'a'*60
    assert a.save == true
    a.search_for = ''
    assert a.save == false
    a.search_for = 'a'*61
    assert a.save == false
  end

  def test_replace_with
    a = Censoring.new
    a.search_for = 'a'
    assert a.save == true
    a.replace_with = 'a'*60
    assert a.save == true
    a.replace_with = 'a'*61
    assert a.save == false
  end
end
