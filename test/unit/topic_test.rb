require File.dirname(__FILE__) + '/../test_helper'

require 'user'
require 'topic'
require 'forum'

class TopicTest < Test::Unit::TestCase
  fixtures :topics, :forums, :users, :groups, :posts, :settings

  def test_forum
    assert_equal topics(:f11).forum.forum_name, 'Forum 1'
    assert_equal topics(:f12).forum.forum_name, 'Forum 1'
    assert_equal topics(:f21).forum.forum_name, 'Forum 2'
    assert_equal topics(:f22).forum.forum_name, 'Forum 2'
  end

  def test_posts
    assert_equal topics(:f11).posts.length, 2
    assert_equal topics(:f12).posts.length, 1
    assert_equal topics(:f21).posts.length, 0
    assert_equal topics(:f22).posts.length, 0
  end

  def test_save
    Settings.subject_all_caps = true
    Settings.group_ids = {:pun_mod => 1}
    User.current_user = User.new
    User.current_user.group = Group.new
    User.current_user.group.id = 2
    t = Topic.new
    assert_equal false, t.save
    t.subject = ''
    assert_equal false, t.save
    t.subject = 'A'*71
    assert_equal false, t.save
    t.subject = 'A'*70
    assert_equal false, t.save
    t.forum = Forum.find_by_id 1
    assert_equal true, t.save
    t.subject = 'A'
    assert_equal true, t.save
  end

  def test_subject_assignment
    Settings.subject_all_caps = true
    Settings.group_ids = {:pun_mod => 1}
    User.current_user = User.new
    User.current_user.group = Group.new
    User.current_user.group.id = 2

    t = Topic.new

    t.subject = '  subject with surrounding spaces  '
    assert_equal t.subject, 'subject with surrounding spaces'

    t.subject = '  UPPER CASE SUBJECT  '
    assert_equal t.subject,'UPPER CASE SUBJECT'

    Settings.subject_all_caps = false

    t.subject = '  subject with surrounding spaces  '
    assert_equal t.subject, 'subject with surrounding spaces'

    t.subject = '  UPPER CASE SUBJECT  '
    assert_equal t.subject,'Upper case subject'

    User.current_user.group.id = 1

    t.subject = '  subject with surrounding spaces  '
    assert_equal t.subject, 'subject with surrounding spaces'

    t.subject = '  UPPER CASE SUBJECT  '
    assert_equal t.subject,'UPPER CASE SUBJECT'
  end

  def test_update_on_post
    p = Post.find_by_id 3
    t = Topic.find_by_id 1
    assert_raise(ArgumentError) { t.update_on_post(p) }

    p = Post.find_by_id 1
    t.update_on_post(p)
    assert_equal t.num_replies, 1
    assert_equal t.last_post, p.updated_at
    assert_equal t.last_post_id, p.id
    assert_equal t.last_poster, p.poster
  end

  def test_forum_validation
    f = forums(:f1)
    assert f.valid?
    t = Topic.new :subject => 'test'
    t.forum = f
    assert t.valid?
    t.forum = nil
    assert_equal false, t.valid?
    t.build_forum()
    t.forum.forum_name = 'test'
    assert_equal false, t.valid?
    t.forum.build_category :cat_name => 'haha'
    assert_equal true, t.forum.valid?
    assert_equal true, t.valid?
    assert t.save
    assert_equal false, t.forum.new_record?
    assert t.forum.id != nil
  end

  def test_mass_assignment
    params = {:subject => 'test', :forum_id => 3}
    t = Topic.new params
    assert_equal t.subject, 'test'
    assert_equal t.forum_id, 0
  end

  def test_can_reply
    t = Topic.new
    t.forum = Forum.new
    t.forum.redirect_url = 'blah'
    for closed in [false,true]
      t.closed = closed
      for u in [users(:guest), users(:mod), users(:admin)]
        assert_equal false, t.can_reply?(u)
        uu = u.clone
        uu.group = groups(:can_post_replies)
        assert_equal false, t.can_reply?(uu)
        uu.group = groups(:cant_post_replies)
        assert_equal false, t.can_reply?(uu)
      end
    end
    t.forum.moderators = {users(:mod).id => 'The mighty mod'}
    for url in ['',nil]
      t.forum.redirect_url = url
      for closed in [false,true]
        t.closed = closed
        assert_equal true, t.can_reply?(users(:admin))
        assert_equal true, t.can_reply?(users(:mod))
      end
      uu = users(:guest).clone
      t.closed = false
      uu.group = groups(:can_post_replies)
      assert_equal true, t.can_reply?(uu)
      uu.group = groups(:cant_post_replies)
      assert_equal false, t.can_reply?(uu)
      t.closed = true
      uu.group = groups(:can_post_replies)
      assert_equal false, t.can_reply?(uu)
      uu.group = groups(:cant_post_replies)
      assert_equal false, t.can_reply?(uu)
    end
  end

  def test_delete
    topics(:f11).destroy
    assert_raise(ActiveRecord::RecordNotFound) {Post.find_by_id posts(:p1).id}
    assert_raise(ActiveRecord::RecordNotFound) {Post.find_by_id posts(:p2).id}
    assert_not_nil Post.find_by_id(posts(:p3).id)
  end

  def test_poster
    t = Topic.new
    t.forum = forums(:f1)
    t.subject = 'a'
    assert_equal true, t.save
    t.poster = 'a'*200
    assert_equal true, t.save
    t.poster = 'a'*201
    assert_equal false, t.save
  end

  def test_poster
    t = Topic.new
    t.forum = forums(:f1)
    t.subject = 'a'
    assert_equal true, t.save
    t.last_poster = 'a'*200
    assert_equal true, t.save
    t.last_poster = 'a'*201
    assert_equal false, t.save
  end

  def test_get_post_position
    t = topics(:f11)
    assert_equal 0,t.get_post_position(posts(:p1))
    assert_equal 1,t.get_post_position(posts(:p2))
    t = topics(:f12)
    assert_equal 0,t.get_post_position(posts(:p3))
  end
end
