require File.dirname(__FILE__) + '/../test_helper'
require 'category_controller'

# Re-raise errors caught by the controller.
class CategoryController; def rescue_action(e) raise e end; end

class CategoryControllerTest < Test::Unit::TestCase
  fixtures :settings, :groups, :users, :forums, :categories
  def setup
    @controller = CategoryController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
    User.current_user = users(:admin)
  end

  def test_categories
    get :index
    assert_response :success
    assert_form :action => 'index'
    assert_tag :tag => 'input', :attributes => {:type => 'submit', :name => 'add_cat'}
    assert_tag :tag => 'input', :attributes => {:type => 'submit', :name => 'update'}
    assert_tag :tag => 'input', :attributes => {:type => 'submit', :name => 'del_cat'}

    get :index, {:add_cat => true, :category => {:cat_name => 'testi'}}
    assert_redirected_to :controller => 'category', :action => 'index'
    assert flash.key?(:error) == false
    c = Category.find_by_cat_name('testi')
    assert_not_nil c

    get :index, {:update => true, :cat => { "#{c.id}" => { :cat_name => 'testtest', :disp_position => 2}}}
    assert_redirected_to :controller => 'category', :action => 'index'
    assert flash.key?(:error) == false
    c.reload
    assert_equal c.cat_name, 'testtest'
    assert_equal c.disp_position, 2

    get :index, {:del_cat => true, :cat_to_delete => c.id}
    assert_response :success
    assert flash.key?(:error) == false
    assert_template 'category/delete'

    get :destroy, {:id => c.id}
    assert_redirected_to :controller => 'category', :action => 'index'
    assert flash.key?(:error) == false
    assert_nil Category.find_by_id(c.id)
  end

end
