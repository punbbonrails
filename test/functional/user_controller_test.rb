require File.dirname(__FILE__) + '/../test_helper'
require 'user_controller'

# Re-raise errors caught by the controller.
class UserController; def rescue_action(e) raise e end; end

class UserControllerTest < Test::Unit::TestCase
  fixtures :settings, :users, :groups, :forums, :categories, :topics, :posts

  def setup
    @controller = UserController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
  end

  def test_list
    get :list
    assert_response :success
    get :list, :username => 'a'
    assert_response :success
    get :list, :username => '*a*'
    assert_response :success
    get :list, :show_group => '1'
    assert_response :success
  end

  def test_view
    get :view, :id => users(:u100).id
    assert_response :success
  end

  def test_register
    get :new
    assert_response :success
    assert_template 'user/new'
    assert_form :action => 'create'
  end

  def test_create
    get :create, :user => {:username => 'bababa', :passwd => 'bababa', :passwd_confirmation => 'bababa', :email => 'bobobo@bobo.com', :email_confirmation => 'bobobo@bobo.com'}
    assert_redirected_to :controller => 'forum'
    assert_not_nil User.find_by_username('bababa')
  end

  def test_logout
    User.current_user = users(:u100)
    get :logout
    assert_redirected_to :controller => 'forum'
    assert_nil User.current_user # it is nil till the next request
  end

  def test_login
    get :create, :user => {:username => 'bababa', :passwd => 'bababa', :passwd_confirmation => 'bababa', :email => 'bobobo@bobo.com', :email_confirmation => 'bobobo@bobo.com'}
    assert_redirected_to :controller => 'forum'
    assert_not_nil User.find_by_username('bababa')
    get :login
    assert_response :success
    assert_form :action => 'login'

    post :login, :username => 'bababa', :password => 'bababa'
    assert_redirected_to :controller => 'forum'
    assert_equal User.find_by_username('bababa'),User.current_user
  end
end
