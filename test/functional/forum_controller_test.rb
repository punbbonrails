require File.dirname(__FILE__) + '/../test_helper'
require 'forum_controller'

# Re-raise errors caught by the controller.
class ForumController; def rescue_action(e) raise e end; end

class ForumControllerTest < Test::Unit::TestCase
  fixtures :users, :groups, :settings, :forums, :topics, :categories, :forum_perms, :bans
  def setup
    @controller = ForumController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
  end

  def test_can_read_board
    User.current_user = users(:guest)
    User.current_user.group.g_read_board = true
    get :index
    assert_response :success
    assert assigns(:forums)
    User.current_user = users(:u101)
    assert_raise(PermissionException) {get :index}

    User.current_user = users(:guest)
    User.current_user.group.g_read_board = false
    assert_raise(PermissionException) {get :index}

    User.current_user.group.g_read_board = true
    assert_response :success
    @request.env['REMOTE_ADDR'] = '172.16.32.12'
    assert_raise(PermissionException) {get :index}
  end

  def test_view
    User.current_user = users(:guest)
    assert_raise(ActiveRecord::RecordNotFound) { get :view }
    get :view, :id => 1
    assert flash.empty?
    assert_response :success

    assert_raise(ActiveRecord::RecordNotFound) {get :view, :id => 100}

    fp = ForumPerm.new
    fp.forum_id = 1
    fp.group_id = users(:guest).group.id
    fp.read_forum = false
    fp.save!
    assert_raise(PermissionException) {get :view, :id => 1}
  end
end
