require File.dirname(__FILE__) + '/../test_helper'
require 'admin_controller'

# Re-raise errors caught by the controller.
class AdminController; def rescue_action(e) raise e end; end

class AdminCategoryTest < Test::Unit::TestCase
  fixtures :settings, :groups, :users, :forums, :categories
  def setup
    @controller = AdminController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
    User.current_user = users(:admin)
  end

  def test_index
    get :index
  end

  def test_options
    get :options
    assert_form :action => 'update_options'
  end

  def test_update_options
    get :update_options, :form => {:board_title => 'board title'}
    assert_response :success
    assert_nil flash[:error]
  end

  def test_permissions
    get :permissions
    assert_form :action => 'update_permissions'
  end

  def test_update_permissions
    get :update_permissions, :form => {:message_bbcode => '1'}
    assert_response :success
    assert_nil flash[:error]
  end

  def test_users
    get :users
    assert_form :action => :find_user
  end

  def test_find_user
    get :find_user
    assert assigns(:users)
  end

  def test_ip_stats
    get :ip_stats
    assert assigns(:max_posted)
    assert assigns(:count_posts)
  end

  def test_prune
    get :prune
    assert assigns(:forums)
  end

  def test_show_users
    get :show_users
    assert assigns(:posts)
  end
end
