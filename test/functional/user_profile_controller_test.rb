require File.dirname(__FILE__) + '/../test_helper'
require 'user_profile_controller'

# Re-raise errors caught by the controller.
class UserProfileController; def rescue_action(e) raise e end; end

class UserProfileControllerTest < Test::Unit::TestCase
  fixtures :settings, :users, :groups, :forums, :categories, :topics, :posts

  def setup
    @controller = UserProfileController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
    User.current_user = users(:u100)
  end

  def test_essentials
    get :index
    assert_response :success
    assert_nil flash[:error]
    assert_not_equal 1, users(:u100).timezone
    assert_not_equal 'vi', users(:u100).language
    assert_form :action => 'update_index', :id => users(:u100).id
    get :update_index, :form => {:timezone => '1', :language => 'vi'}
    assert_response :success
    assert_nil flash[:error]
    assert_equal 1, users(:u100).timezone
    assert_equal 'vi', users(:u100).language
  end

  def test_change_password
    u = users(:u100)
    get :change_password
    assert_response :success
    assert_tag :tag => 'input', :attributes => {:type => 'password', :name => 'old_password'}
    assert_form :action => 'update_password', :id => u.id
    u.passwd = 'testz'
    u.passwd_confirmation = 'testz'
    u.valid? or puts u.errors.full_messages.join("\n")
    assert users(:u100).save == true
    get :update_password, :old_password => 'testz', :form => {:passwd => 'ztset', :passwd_confirmation => 'ztset'}
    assert_redirected_to :controller => 'user_profile', :action => 'index'
    assert_nil flash[:error]
    u.reload
    assert u.authenticated?('ztset') == true
  end

  def test_change_email
    u = users(:u100)
    get :change_email
    assert_response :success
    assert_tag :tag => 'input', :attributes => {:type => 'password', :name => 'req_password'}
    assert_form :action => 'update_email', :id => u.id
    u.passwd = 'testz'
    u.passwd_confirmation = 'testz'
    u.valid? or puts u.errors.full_messages.join("\n")
    assert users(:u100).save == true
    get :update_email, :req_password => 'testz', :req_new_email => 'ztset@test.com'
    assert_redirected_to :controller => 'user_profile', :action => 'index'
    assert_nil flash[:error]
    u.reload
    assert_equal u.email, 'ztset@test.com'
  end

  def test_personal
    u = users(:u100)
    get :personal
    assert_response :success
    assert_nil flash[:error]
    assert_form :action => 'update_personal', :id => u.id
    get :update_personal, :form => {:realname => 'test realname', :location => 'test location', :url => 'test website'}
    assert_response :success
    assert_nil flash[:error]
    u.reload
    assert_equal 'test realname', u.realname
    assert_equal 'test location', u.location
    assert_equal 'test website', u.url
  end

  def test_messaging
    u = users(:u100)
    get :messaging
    assert_response :success
    assert_nil flash[:error]
    assert_form :action => 'update_messaging', :id => u.id
    get :update_messaging, :form => {:jabber => 'test jabber', :icq => 'test icq', :msn => 'test msn', :aim => 'test aim', :yahoo => 'test yahoo'}
    assert_response :success
    assert_nil flash[:error]
    u.reload
    assert_equal 'test jabber', u.jabber
    assert_equal 'test icq', u.icq
    assert_equal 'test msn', u.msn
    assert_equal 'test aim', u.aim
    assert_equal 'test yahoo', u.yahoo
  end

  def test_personality
    u = users(:u100)
    get :personality
    assert_response :success
    assert_nil flash[:error]
    assert_form :action => 'update_personality', :id => u.id
    get :update_personality, :form => {:signature => 'test sig'}
    assert_response :success
    assert_nil flash[:error]
    u.reload
    assert_equal 'test sig', u.signature
  end

  def test_appearance
    u = users(:u100)
    get :appearance
    assert_response :success
    assert_nil flash[:error]
    assert_form :action => 'update_appearance', :id => u.id
    get :update_appearance, :form => {:style => 'Cornflower', :show_sig => true, :show_avatars => true, :show_img => true, :show_smilies => true, :show_img_sig => true, :disp_posts => 10, :disp_topics => 20}
    assert_response :success
    assert_nil flash[:error]
    u.reload
    assert_equal 'Cornflower', u.style
    assert_equal true, u.show_smilies
    assert_equal true, u.show_sig
    assert_equal true, u.show_avatars
    assert_equal true, u.show_img
    assert_equal true, u.show_img_sig
    assert_equal 10, u.disp_posts
    assert_equal 20, u.disp_topics
    assert_form :action => 'update_appearance', :id => u.id
    get :update_appearance, :form => {:style => 'Oxygen'}
    assert_response :success
    assert_nil flash[:error]
    u.reload
    assert_equal 'Oxygen', u.style
    assert_equal false, u.show_smilies
    assert_equal false, u.show_sig
    assert_equal false, u.show_avatars
    assert_equal false, u.show_img
    assert_equal false, u.show_img_sig
  end

  def test_privacy
    u = users(:u100)
    get :privacy
    assert_response :success
    assert_nil flash[:error]
    assert_form :action => 'update_privacy', :id => u.id
    get :update_privacy, :form => {:email_setting => '1', :save_pass => true, :notify_with_post => true}
    assert_response :success
    assert_nil flash[:error]
    u.reload
    assert_equal 1, u.email_setting
    assert_equal true, u.save_pass
    assert_equal true, u.notify_with_post
    get :update_privacy, :form => {:email_setting => '2'}
    assert_response :success
    assert_nil flash[:error]
    u.reload
    assert_equal 2, u.email_setting
    assert_equal false, u.save_pass
    assert_equal false, u.notify_with_post
  end
end
