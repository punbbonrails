require File.dirname(__FILE__) + '/../test_helper'
require 'topic_controller'

class TopicController
  # Re-raise errors caught by the controller.
  def rescue_action(e) raise e end
  # disable flood detection
  def flood?() true; end
end

class TopicControllerTest < Test::Unit::TestCase
  fixtures :forums, :topics, :posts, :settings, :groups, :users, :categories
  def setup
    @controller = TopicController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
  end

  def test_construct_post
    User.current_user = users(:admin)
    for a in [:new, :create]
      for p in [{:fid => forums(:f1).id}, {:id => topics(:f11).id}]
        get a, p
        assert assigns(:post) != nil 
        assert assigns(:post).topic != nil
        assert assigns(:post).topic.forum != nil
      end
    end
  end

  def test_create
    User.current_user = users(:admin)
    params = {:fid => forums(:f1).id, :post => {:message => 'test', :hide_smilies => false}, :topic => {:subject => 'test'}}
    post :create, params.merge({:preview => true})
    assert_response :success # render :action => new
    post :create, params
    assert_redirected_to :controller => 'topic', :action => 'view'
    assert assigns(:post) != nil
    assert assigns(:post).new_record? == false
    assert assigns(:post).topic.new_record? == false

    params = {:id => topics(:f11).id, :post => {:message => 'test', :hide_smilies => false}}
    post :create, params.merge({:preview => true})
    assert_response :success
    post :create, params
    assert_redirected_to :controller => 'topic', :action => 'view'
    assert assigns(:post) != nil
    assert assigns(:post).new_record? == false
    assert_equal assigns(:post).topic.id, topics(:f11).id
  end

  def test_update
    User.current_user = users(:admin)
    params = {:pid => posts(:p1).id, :post => {:message => 'test', :hide_smilies => false} }
    post :update, params.merge({:preview => true})
    assert_response :success
    post :update, params
    assert_redirected_to :controller => 'topic', :action => 'view'
  end

  def test_view
    get :view, :id => topics(:f11).id
    assert_response :success

    get :view, :pid => posts(:p1).id
    assert_response :success
  end
end
