require File.dirname(__FILE__) + '/../test_helper'
require 'ban_controller'

# Re-raise errors caught by the controller.
class BanController; def rescue_action(e) raise e end; end

class BanControllerTest < Test::Unit::TestCase
  fixtures :settings, :groups, :users, :forums, :categories, :bans
  def setup
    @controller = BanController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
    User.current_user = users(:admin)
  end

  def test_index
    get :index
    assert_response :success
    assert_form :action => 'new'
  end

  def test_create
    get :new
    assert_response :success
    assert_template 'ban/new'

    get :new, :ban => {:username => 'user one hundred'}
    assert_redirected_to :controller => 'ban', :action => 'index'
    assert_not_nil Ban.find_by_username('user one hundred')

    get :new, :ban => {:email => 'test@email.com'}
    assert_redirected_to :controller => 'ban', :action => 'index'
    assert_not_nil Ban.find_by_email('test@email.com')

    get :new, :ban => {:ip => '123.32'}
    assert_redirected_to :controller => 'ban', :action => 'index'
    assert_not_nil Ban.find_by_ip('123.32')
  end

  def test_update
    b = bans(:u100)
    get :edit, :id => b.id
    assert_response :success
    assert_form :action => 'update', :id => b.id
    get :update, :id => b.id, :ban => {:username => b.username, :email => 'test@email.com'}
    assert_redirected_to :controller => 'ban', :action => 'index'
    b.reload
    assert_equal 'test@email.com', b.email
  end

  def test_destroy
    b = bans(:u100)
    get :destroy, :id => b.id
    assert_redirected_to :controller => 'ban', :action => 'index'
    assert_nil Ban.find_by_id(b.id)
  end
end
