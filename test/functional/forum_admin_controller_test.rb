require File.dirname(__FILE__) + '/../test_helper'
require 'forum_admin_controller'

# Re-raise errors caught by the controller.
class ForumAdminController; def rescue_action(e) raise e end; end

class ForumAdminControllerTest < Test::Unit::TestCase
  fixtures :settings, :groups, :users, :forums, :categories
  def setup
    @controller = ForumAdminController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
    User.current_user = users(:admin)
  end

  def test_forums
    get :index
    assert_response :success
    assert_form :action => 'update_index'
    assert_tag :tag => 'input', :attributes => {:type => 'submit', :name => 'add_forum'}

    c = Category.new({:cat_name => 'aaa'})
    assert c.save == true
    get :update_index, {:add_forum => true, :forum => {:forum_name => 'testi', :cat_id => c.id}}
    assert_redirected_to :controller => 'forum_admin', :action => 'index'
    assert flash.key?(:error) == false
    f = Forum.find_by_forum_name('testi')
    assert_not_nil f
    assert_equal f.cat_id,c.id

    get :update_index, {:position => { "#{f.id}" => "2"}}
    assert_redirected_to :controller => 'forum_admin', :action => 'index'
    assert flash.key?(:error) == false
    f.reload
    assert_equal f.disp_position, 2

    get :edit, {:id => f.id}
    assert_response :success

    post :update, {:id => f.id, :forum => {:forum_name => 'testtest'}}
    assert_redirected_to :controller => 'forum_admin', :action => 'index'
    f.reload
    assert_equal f.forum_name, 'testtest'

    post :update, {:id => f.id, :forum => {:forum_name => 'testtest'}, :forum_perms => {}}
    assert_redirected_to :controller => 'forum_admin', :action => 'index'
    assert_equal 11, ForumPerm.count("forum_id=#{f.id}")

    post :update, {:id => f.id, :revert_perms => true}
    assert_redirected_to :controller => 'forum_admin', :action => 'index'
    assert_equal 0, ForumPerm.count("forum_id=#{f.id}")

    get :delete, {:id => f.id}
    assert_response :success
    assert_template 'forum_admin/delete'
    assert_form :action => 'destroy', :id => f.id

    get :destroy, {:id => f.id}
    assert_redirected_to :controller => 'forum_admin', :action => 'index'
    assert flash.key?(:error) == false
    assert_nil Forum.find_by_id(f.id)
  end
end
