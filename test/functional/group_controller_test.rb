require File.dirname(__FILE__) + '/../test_helper'
require 'group_controller'

# Re-raise errors caught by the controller.
class GroupController; def rescue_action(e) raise e end; end

class GroupControllerTest < Test::Unit::TestCase
  fixtures :settings, :groups, :users, :forums, :categories
  def setup
    @controller = GroupController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
    User.current_user = users(:admin)
  end

  def test_group_edit
    g = groups(:guest)
    id = g.id
    get :edit, :id => id
    assert_response :success
    assert flash.key?(:errors) == false

    post :update, :id => id, :group => {:g_title => 'test title', :g_user_title => 'test user title'}
    assert_redirected_to :controller => 'group', :action => 'index'
    g.reload
    assert_equal g.g_title, 'test title'
    assert_equal g.g_user_title, 'test user title'
    
    for set in [{:group => groups(:member), :keys => [:g_read_board, :g_post_replies, :g_post_topics, :g_post_polls, :g_edit_posts, :g_delete_posts, :g_delete_topics, :g_set_title, :g_search, :g_search_users]}, {:group => groups(:guest), :keys => [:g_read_board, :g_post_replies, :g_post_topics, :g_post_polls, :g_search, :g_search_users]}]
      g = set[:group]
      id = g.id
      keys = set[:keys]
      for result in [{:value => '0', :bool => false},{:value => '1', :bool => true}]
        post :update, :id => id, :group => keys.inject(Hash.new) {|h,k| h[k] = result[:value]; h}
        assert_redirected_to :controller => 'group', :action => 'index'
        g.reload
        keys.each { |k| assert g.send(k) == result[:bool] }
      end
    end

    g = groups(:member)
    id = g.id
    post :update, :id => id, :group => {:g_edit_subjects_interval => '10', :g_post_flood => '12', :g_search_flood => '14'}
    assert_redirected_to :controller => 'group', :action => 'index'
    g.reload
    assert_equal 10, g.g_edit_subjects_interval
    assert_equal 12, g.g_post_flood
    assert_equal 14, g.g_search_flood
  end

  def test_groups
    get :index
    assert_response :success

    get :update_index, :add_group => true, :base_group => groups(:g100).id
    assert_redirected_to :controller => 'group', :action => 'index'
    assert_nil flash[:error]
    g = Group.find_by_g_title('group one (new)')
    assert_not_nil g
    assert g.id != groups(:g100).id

    get :update_index, :set_default_group => true, :default_group => g.id
    assert_redirected_to :controller => 'group', :action => 'index'
    assert_nil flash[:error]
    assert_equal Settings.default_user_group, g.id

    assert_raise(PermissionException,'You can not delete default group') {get :delete, :id => g.id}

    get :update_index, :set_default_group => true, :default_group => groups(:member).id
    assert_redirected_to :controller => 'group', :action => 'index'
    assert_nil flash[:error]
    assert_equal Settings.default_user_group, groups(:member).id

    u = User.new
    u.username = 'memem'
    u.email = 'meme@meme.com'
    u.email_confirmation = u.email
    u.password = 'test'
    u.group = g
    assert u.save == true
    assert_equal u.group_id,g.id
    get :delete, :id => g.id
    assert_response :success
    assert_form :action => 'destroy', :id => g.id

    get :destroy, :id => g.id, :move_to_group => groups(:g100).id
    assert_redirected_to :controller => 'group', :action => 'index'
    assert_nil flash[:error]
    assert_nil Group.find_by_id(g.id)
    u.reload
    assert_equal u.group_id,groups(:g100).id
  end
end
