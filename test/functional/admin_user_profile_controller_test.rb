require File.dirname(__FILE__) + '/../test_helper'
require 'user_profile_controller'

# Re-raise errors caught by the controller.
class UserProfileController; def rescue_action(e) raise e end; end

class AdminUserProfileControllerTest < Test::Unit::TestCase
  fixtures :settings, :users, :groups, :forums, :categories, :topics, :posts

  def setup
    @controller = UserProfileController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
    User.current_user = users(:admin)
  end

  def test_group_admin
    get :update_group, :id => User.current_user.id, :group_id => 1
    assert_redirected_to :controller => 'user_profile', :action => 'admin', :id => User.current_user.id.to_s
    assert_not_equal 1, User.find_by_id(users(:u100).id).group.id
    get :admin, :id => users(:u100).id, :update_group_membership => true, :group_id => 1
    assert_redirected_to :controller => 'user_profile', :action => 'update_group'
    get :update_group, :id => users(:u100).id, :group_id => 1
    assert_redirected_to :controller => 'user_profile', :action => 'admin', :id => users(:u100).id.to_s
    assert_nil flash[:error]
    assert_equal 1, User.find_by_id(users(:u100).id).group.id
  end

  def test_delete
    get :admin, :id => users(:u100).id, :delete_user => true
    assert_response :success
    assert_template 'user_profile/delete_user_confirm'
  end

  def test_delete_admin
    assert_raise PermissionException, 'Administrators can not be deleted' do
      get :delete, :id => users(:admin).id
    end
  end

  def test_delete_keeping_posts
    get :delete, :id => users(:u100).id
    assert_redirected_to :controller => 'admin'
    assert_nil flash[:error]
    assert_nil User.find_by_id(users(:u100).id)
  end

  def test_delete_with_posts
    uid = users(:u100).id
    pid1 = posts(:p2).id
    pid2 = posts(:p3).id
    tid = topics(:f12).id
    get :delete, :id => uid, :delete_posts => true
    assert_redirected_to :controller => 'admin'
    assert_nil flash[:error]
    assert_nil User.find_by_id(uid)
    assert_nil Post.find_by_id(pid1)
    assert_nil Post.find_by_id(pid2)
    assert_nil Topic.find_by_id(tid)
    assert_not_nil Post.find_by_id(posts(:p1).id)
    assert_not_nil Topic.find_by_id(topics(:f11).id)
    assert_not_nil Topic.find_by_id(topics(:f21).id)
    assert_not_nil Topic.find_by_id(topics(:f22).id)
  end

  def test_ban
    get :admin, :id => users(:u100).id, :ban => true
    assert_redirected_to :controller => 'ban', :action => 'create', 'ban[username]' => users(:u100).username
  end

  def test_moderator
    get :admin, :id => users(:mod).id, :update_forums => true
    assert_response :success
    assert_not_nil flash[:notice]
    forums = Forum.find_all
    forums.each {|f| assert_equal false, f.moderators.include?(users(:mod).id)}

    get :admin, :id => users(:mod).id, :update_forums => true, :moderator_in => [forums(:f1).id.to_s]
    assert_response :success
    assert_not_nil flash[:notice]
    f = forums(:f1)
    f.reload
    assert_equal true, f.moderators.include?(users(:mod).id)
    forums = Forum.find_all "id != #{forums(:f1).id}"
    forums.each {|f| assert_equal false, f.moderators.include?(users(:mod).id)}
  end
end
