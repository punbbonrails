ActionController::Routing::Routes.draw do |map|
  map.home '', :controller => 'forum'
  map.connect ':controller/:action/:id'
end
